load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_TAG = "v2.0.0"

def osi_traffic_participant():
    maybe(
        http_archive,
        name = "osi_traffic_participant",
        url =  "https://gitlab.eclipse.org/eclipse/openpass/osi-traffic-participant/-/archive/{tag}/osi-traffic-participant-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "378f4961ce235850d0035bd611297978bc2dd9d1fe2bc8e23e8d5dfcab342de3",
        strip_prefix = "osi-traffic-participant-{tag}".format(tag = _TAG),
        type = "tar.gz",
    )
