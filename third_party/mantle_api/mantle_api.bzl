load("@gt_gen_core//third_party:upstream_remotes.bzl", "ECLIPSE_GITLAB")
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_TAG = "v5.1.0"

def mantle_api():
    maybe(
        http_archive,
        name = "mantle_api",
        url = ECLIPSE_GITLAB + "/openpass/mantle-api/-/archive/{tag}/mantle-api-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "e5338736c95b6cdc97576780f71c63a5975ca38c487b6e58178e3d0bb2dc074b",
        strip_prefix = "mantle-api-{tag}".format(tag = _TAG),
        type = "tar.gz",
    )
