load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "v1.0.0"

def foxglove_websocket_cpp():
    maybe(
        http_archive,
        name = "foxglove_websocket_cpp",
        sha256 = "c6106934a47d9d5b53ee3f6a0a6c8ec778ffdb52e9b462136bfc1bc5391ed227",
        build_file = Label("//:third_party/foxglove_websocket_cpp/foxglove_websocket_cpp.BUILD"),
        url = "https://github.com/foxglove/ws-protocol/archive/refs/tags/releases/cpp/{version}.tar.gz".format(version = _VERSION),
        strip_prefix = "ws-protocol-releases-cpp-{version}".format(version = _VERSION),
    )
