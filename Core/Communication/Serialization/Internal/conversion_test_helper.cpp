/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Serialization/Internal/conversion_test_helper.h"

#include "Core/Communication/Serialization/Internal/map_type_conversions.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"

#include <gtest/gtest.h>

namespace gtgen::core::communication
{

void CheckVector3(const mantle_api::Vec3<units::length::meter_t>& map_vector,
                  const messages::map::Vector3d& proto_vector)
{
    EXPECT_DOUBLE_EQ(proto_vector.x(), map_vector.x());
    EXPECT_DOUBLE_EQ(proto_vector.y(), map_vector.y());
    EXPECT_DOUBLE_EQ(proto_vector.z(), map_vector.z());
}

void CheckDimension3(const mantle_api::Dimension3& gtgen_dimension, const messages::map::Dimension3d& proto_dimension)
{
    EXPECT_DOUBLE_EQ(proto_dimension.width(), gtgen_dimension.width());
    EXPECT_DOUBLE_EQ(proto_dimension.length(), gtgen_dimension.length());
    EXPECT_DOUBLE_EQ(proto_dimension.height(), gtgen_dimension.height());
}

void CheckOrientation3(const mantle_api::Orientation3<units::angle::radian_t>& gtgen_orientation,
                       const messages::map::Orientation3d& proto_orientation)
{
    EXPECT_DOUBLE_EQ(proto_orientation.yaw(), gtgen_orientation.yaw());
    EXPECT_DOUBLE_EQ(proto_orientation.pitch(), gtgen_orientation.pitch());
    EXPECT_DOUBLE_EQ(proto_orientation.roll(), gtgen_orientation.roll());
}

void CheckPose(const mantle_api::Pose& gtgen_pose, const messages::map::Pose& proto_pose)
{
    CheckVector3(gtgen_pose.position, proto_pose.position());
    CheckOrientation3(gtgen_pose.orientation, proto_pose.orientation());
}

void CheckRoadObject(const environment::map::RoadObject& gtgen_object, const messages::map::RoadObject& proto_object)
{
    EXPECT_EQ(proto_object.id(), gtgen_object.id);
    EXPECT_EQ(proto_object.name(), gtgen_object.name);
    EXPECT_DOUBLE_EQ(proto_object.height(), gtgen_object.dimensions.height());
    EXPECT_EQ(proto_object.type(), GtGenToProtoRoadObjectType(gtgen_object.type));
    CheckPose(gtgen_object.pose, proto_object.pose());
    CheckDimension3(gtgen_object.dimensions, proto_object.dimensions());
}

void CheckSupplementarySign(const environment::map::MountedSign::SupplementarySign& gtgen_sign,
                            const messages::map::TrafficSign& proto_sign)
{
    CheckPose(gtgen_sign.pose, proto_sign.pose());
    CheckDimension3(gtgen_sign.dimensions, proto_sign.dimension());

    ASSERT_FALSE(proto_sign.value_information().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_sign.value_information_size()), gtgen_sign.value_information.size());
    EXPECT_EQ(proto_sign.value_information(0).text(), gtgen_sign.value_information[0].text);
    EXPECT_DOUBLE_EQ(proto_sign.value_information(0).value(), gtgen_sign.value_information[0].value);
    EXPECT_EQ(proto_sign.value_information(0).unit(), GtGenToProtoSignUnit(gtgen_sign.value_information[0].value_unit));

    EXPECT_EQ(proto_sign.sign_variability(), GtGenToProtoSignVariability(gtgen_sign.variability));
    EXPECT_EQ(proto_sign.supplementary_sign_type(), GtGenToProtoSupplementarySign(gtgen_sign.type));
}

void CheckTrafficSign(const environment::map::TrafficSign& gtgen_sign, const messages::map::TrafficSign& proto_sign)
{
    EXPECT_EQ(proto_sign.id(), gtgen_sign.id);
    EXPECT_EQ(proto_sign.stvo_id(), gtgen_sign.stvo_id);
    EXPECT_EQ(proto_sign.sign_variability(), GtGenToProtoSignVariability(gtgen_sign.variability));
    EXPECT_EQ(proto_sign.direction_scope(), GtGenToProtoDirectionScope(gtgen_sign.direction_scope));
    EXPECT_EQ(proto_sign.main_sign_type(), GtGenToProtoMainSign(gtgen_sign.type));

    ASSERT_FALSE(proto_sign.value_information().empty());
    EXPECT_EQ(proto_sign.value_information(0).text(), gtgen_sign.value_information.text);
    EXPECT_DOUBLE_EQ(proto_sign.value_information(0).value(), gtgen_sign.value_information.value);
    EXPECT_EQ(proto_sign.value_information(0).unit(), GtGenToProtoSignUnit(gtgen_sign.value_information.value_unit));

    ASSERT_FALSE(proto_sign.assigned_lanes().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_sign.assigned_lanes_size()), gtgen_sign.assigned_lanes.size());
    EXPECT_EQ(proto_sign.assigned_lanes(0), gtgen_sign.assigned_lanes[0]);

    CheckPose(gtgen_sign.pose, proto_sign.pose());
    CheckDimension3(gtgen_sign.dimensions, proto_sign.dimension());
}

void CheckMountedSign(const environment::map::MountedSign& gtgen_sign, const messages::map::MountedSign& proto_sign)
{
    CheckTrafficSign(gtgen_sign, proto_sign.sign());

    ASSERT_FALSE(proto_sign.supplementary_signs().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_sign.supplementary_signs_size()), gtgen_sign.supplementary_signs.size());
    CheckSupplementarySign(gtgen_sign.supplementary_signs[0], proto_sign.supplementary_signs(0));
}

void CheckGroundSign(const environment::map::GroundSign& gtgen_sign, const messages::map::GroundSign& proto_sign)
{
    CheckTrafficSign(gtgen_sign, proto_sign.sign());

    EXPECT_EQ(proto_sign.road_marking_type(), GtGenToProtoRoadMarkingType(gtgen_sign.marking_type));
    EXPECT_EQ(proto_sign.road_marking_color(), GtGenToProtoRoadMarkingColor(gtgen_sign.marking_color));
}

void CheckTrafficLightBulb(const environment::map::TrafficLightBulb& gtgen_traffic_light_bulb,
                           const messages::map::TrafficLight_LightBulb& proto_traffic_light_bulb)
{
    EXPECT_EQ(proto_traffic_light_bulb.id(), gtgen_traffic_light_bulb.id);
    EXPECT_EQ(proto_traffic_light_bulb.color(), GtGenToProtoOsiTrafficLightColor(gtgen_traffic_light_bulb.color));
    EXPECT_EQ(proto_traffic_light_bulb.icon(), GtGenToProtoOsiTrafficLightIcon(gtgen_traffic_light_bulb.icon));
    EXPECT_EQ(proto_traffic_light_bulb.mode(), GtGenToProtoOsiTrafficLightMode(gtgen_traffic_light_bulb.mode));

    if (gtgen_traffic_light_bulb.mode == environment::map::OsiTrafficLightMode::kCounting)
    {
        EXPECT_DOUBLE_EQ(proto_traffic_light_bulb.value(), gtgen_traffic_light_bulb.count);
    }

    ASSERT_EQ(static_cast<std::size_t>(proto_traffic_light_bulb.assigned_lanes().size()),
              gtgen_traffic_light_bulb.assigned_lanes.size());
    for (std::size_t i{0}; i < gtgen_traffic_light_bulb.assigned_lanes.size(); ++i)
    {
        EXPECT_EQ(proto_traffic_light_bulb.assigned_lanes(static_cast<int>(i)),
                  gtgen_traffic_light_bulb.assigned_lanes[i]);
    }

    CheckPose(gtgen_traffic_light_bulb.pose, proto_traffic_light_bulb.pose());
    CheckDimension3(gtgen_traffic_light_bulb.dimensions, proto_traffic_light_bulb.dimension());
}

void CheckTrafficLight(const environment::map::TrafficLight& gtgen_traffic_light,
                       const messages::map::TrafficLight& proto_traffic_light)
{
    EXPECT_EQ(proto_traffic_light.id(), gtgen_traffic_light.id);

    ASSERT_EQ(gtgen_traffic_light.light_bulbs.size(),
              static_cast<std::size_t>(proto_traffic_light.light_bulbs().size()));
    for (std::size_t i{0}; i < gtgen_traffic_light.light_bulbs.size(); ++i)
    {
        CheckTrafficLightBulb(gtgen_traffic_light.light_bulbs[i], proto_traffic_light.light_bulbs(static_cast<int>(i)));
    }
}

void CheckLaneBoundary(const environment::map::LaneBoundary& gtgen_boundary,
                       const messages::map::LaneBoundary& proto_boundary)
{
    EXPECT_EQ(proto_boundary.id(), gtgen_boundary.id);
    EXPECT_EQ(proto_boundary.mirrored_from(), gtgen_boundary.mirrored_from);
    EXPECT_EQ(proto_boundary.parent_lane_group(), gtgen_boundary.parent_lane_group_id);
    EXPECT_EQ(proto_boundary.color(), GtGenToProtoBoundaryColor(gtgen_boundary.color));
    EXPECT_EQ(proto_boundary.type(), GtGenToProtoBoundaryType(gtgen_boundary.type));

    ASSERT_FALSE(proto_boundary.points().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_boundary.points_size()), gtgen_boundary.points.size());
    for (std::size_t i = 0; i < gtgen_boundary.points.size(); ++i)
    {
        const auto& gtgen_boundary_point = gtgen_boundary.points[i];
        const auto& proto_boundary_point = proto_boundary.points(static_cast<int>(i));
        EXPECT_DOUBLE_EQ(proto_boundary_point.width(), gtgen_boundary_point.width);
        EXPECT_DOUBLE_EQ(proto_boundary_point.height(), gtgen_boundary_point.height);
        CheckVector3(gtgen_boundary_point.position, proto_boundary_point.position());
    }
}

void CheckLane(const environment::map::Lane& gtgen_lane, const messages::map::Lane& proto_lane)
{
    EXPECT_EQ(proto_lane.id(), gtgen_lane.id);
    EXPECT_EQ(proto_lane.type(), GtGenToProtoLaneType(gtgen_lane.flags));
    EXPECT_EQ(proto_lane.parent_lane_group(), gtgen_lane.parent_lane_group_id);

    ASSERT_FALSE(proto_lane.hd_center_line().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_lane.hd_center_line_size()), gtgen_lane.center_line.size());
    for (std::size_t i = 0; i < gtgen_lane.center_line.size(); ++i)
    {
        CheckVector3(gtgen_lane.center_line[i], proto_lane.hd_center_line(static_cast<int>(i)));
    }

    ASSERT_FALSE(proto_lane.predecessors().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_lane.predecessors_size()), gtgen_lane.predecessors.size());
    for (std::size_t i = 0; i < gtgen_lane.predecessors.size(); ++i)
    {
        EXPECT_EQ(proto_lane.predecessors(static_cast<int>(i)), gtgen_lane.predecessors[i]);
    }
    ASSERT_FALSE(proto_lane.successors().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_lane.successors_size()), gtgen_lane.successors.size());
    for (std::size_t i = 0; i < gtgen_lane.successors.size(); ++i)
    {
        EXPECT_EQ(proto_lane.successors(static_cast<int>(i)), gtgen_lane.successors[i]);
    }

    ASSERT_FALSE(proto_lane.left_adjacent_lanes().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_lane.left_adjacent_lanes_size()), gtgen_lane.left_adjacent_lanes.size());
    for (std::size_t i = 0; i < gtgen_lane.left_adjacent_lanes.size(); ++i)
    {
        EXPECT_EQ(proto_lane.left_adjacent_lanes(static_cast<int>(i)), gtgen_lane.left_adjacent_lanes[i]);
    }
    ASSERT_FALSE(proto_lane.right_adjacent_lanes().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_lane.right_adjacent_lanes_size()), gtgen_lane.right_adjacent_lanes.size());
    for (std::size_t i = 0; i < gtgen_lane.right_adjacent_lanes.size(); ++i)
    {
        EXPECT_EQ(proto_lane.right_adjacent_lanes(static_cast<int>(i)), gtgen_lane.right_adjacent_lanes[i]);
    }

    ASSERT_FALSE(proto_lane.left_lane_boundaries().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_lane.left_lane_boundaries_size()), gtgen_lane.left_lane_boundaries.size());
    for (std::size_t i = 0; i < gtgen_lane.left_lane_boundaries.size(); ++i)
    {
        EXPECT_EQ(proto_lane.left_lane_boundaries(static_cast<int>(i)), gtgen_lane.left_lane_boundaries[i]);
    }
    ASSERT_FALSE(proto_lane.right_lane_boundaries().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_lane.right_lane_boundaries_size()),
              gtgen_lane.right_lane_boundaries.size());
    for (std::size_t i = 0; i < gtgen_lane.right_lane_boundaries.size(); ++i)
    {
        EXPECT_EQ(proto_lane.right_lane_boundaries(static_cast<int>(i)), gtgen_lane.right_lane_boundaries[i]);
    }
}

void CheckLaneGroup(const environment::map::GtGenMap& gtgen_map,
                    const environment::map::LaneGroup& gtgen_group,
                    const messages::map::LaneGroup& proto_group)
{
    EXPECT_EQ(proto_group.id(), gtgen_group.id);
    EXPECT_EQ(proto_group.type(), GtGenToProtoLaneGroupType(gtgen_group.type));

    ASSERT_FALSE(proto_group.lane_boundaries().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_group.lane_boundaries_size()), gtgen_group.lane_boundary_ids.size());
    const auto& gtgen_boundary = gtgen_map.GetLaneBoundary(gtgen_group.lane_boundary_ids[0]);
    CheckLaneBoundary(gtgen_boundary, proto_group.lane_boundaries(0));

    ASSERT_FALSE(proto_group.lanes().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_group.lanes_size()), gtgen_group.lane_ids.size());
    const auto& gtgen_lane = gtgen_map.GetLane(gtgen_group.lane_ids[0]);
    CheckLane(gtgen_lane, proto_group.lanes(0));
}

}  // namespace gtgen::core::communication
