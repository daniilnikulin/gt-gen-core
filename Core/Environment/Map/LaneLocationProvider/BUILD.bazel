cc_library(
    name = "lane_finder",
    srcs = ["lane_finder.cpp"],
    hdrs = ["lane_finder.h"],
    deps = [
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "//Core/Service/Profiling:profiling",
    ],
)

cc_test(
    name = "lane_finder_test",
    timeout = "short",
    srcs = ["lane_finder_test.cpp"],
    deps = [
        ":lane_finder",
        "//Core/Tests/TestUtils/MapUtils:gtgen_map_builder",
        "@googletest//:gtest_main",
    ],
)

# TODO: test missing
cc_library(
    name = "lane_location",
    srcs = ["lane_location.cpp"],
    hdrs = ["lane_location.h"],
    visibility = ["//Core/Environment/LaneFollowing:__subpackages__"],
    deps = [
        "//Core/Environment/Map/Geometry:project_query_point_on_boundaries",
        "//Core/Environment/Map/Geometry:project_query_point_on_polyline",
        "//Core/Environment/Map/GtGenMap:lane",
        "//Core/Environment/Map/GtGenMap:lane_boundary",
        "//Core/Environment/Map/GtGenMap:utility",
        "//Core/Service/GlmWrapper:glm_wrapper",
        "//Core/Service/Profiling:profiling",
        "@mantle_api",
    ],
)

cc_library(
    name = "lane_location_provider",
    srcs = ["lane_location_provider.cpp"],
    hdrs = ["lane_location_provider.h"],
    visibility = [
        "//Core/Communication/Dispatchers:__subpackages__",
        "//Core/Environment:__subpackages__",
        "//Core/Tests:__subpackages__",
    ],
    deps = [
        ":lane_finder",
        ":lane_location",
        "//Core/Environment/Exception:exception",
        "//Core/Environment/GtGenEnvironment/Internal:geometry_helper",
        "//Core/Environment/LaneFollowing:find_longest_path",
        "//Core/Environment/LaneFollowing:line_follow_data_from_lanes",
        "//Core/Environment/LaneFollowing:line_follow_data_from_path",
        "//Core/Environment/LaneFollowing:point_list_traverser",
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "//Core/Environment/Map/GtGenMap:lane",
        "//Core/Environment/PathFinding:path",
        "//Core/Service/GlmWrapper:glm_basic_orientation_utils",
        "//Core/Service/GlmWrapper:glm_basic_vector_utils",
        "//Core/Service/Logging:logging",
        "//Core/Service/Profiling:profiling",
        "//Core/Service/Utility:math_utils",
        "//Core/Service/Utility:position_utils",
        "@mantle_api",
    ],
)

cc_test(
    name = "lane_location_provider_test",
    timeout = "short",
    srcs = ["lane_location_provider_test.cpp"],
    deps = [
        ":lane_location_provider",
        "//Core/Environment/Exception:exception",
        "//Core/Environment/Map/Geometry:bounding_box",
        "//Core/Environment/Map/GtGenMap:gtgen_map_finalizer",
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "//Core/Service/GlmWrapper:glm_basic_vector_utils",
        "//Core/Tests/TestUtils:expect_extensions",
        "//Core/Tests/TestUtils/MapCatalogue:lane_group_catalogue",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue_id_provider",
        "//Core/Tests/TestUtils/MapCatalogue:raw_map_builder",
        "@googletest//:gtest",
        "@googletest//:gtest_main",
    ],
)
