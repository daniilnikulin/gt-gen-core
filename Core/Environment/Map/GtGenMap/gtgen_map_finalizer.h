/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_GTGENMAPFINALIZER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_GTGENMAPFINALIZER_H

#include "Core/Environment/Map/GtGenMap/gtgen_map.h"

namespace gtgen::core::environment::map
{
class GtGenMapFinalizer
{
  public:
    explicit GtGenMapFinalizer(GtGenMap& gtgen_map);

    void Finalize();

  protected:
    void RemoveDuplicatedSuccessorAndPredecessors();
    void ExtractLaneShapes();
    void InitWorldBoundingBox();
    void BuildSpatialHash();
    void FixBrokenLaneConnectionsInTileCrossing();

    GtGenMap& gtgen_map_;
};

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_GTGENMAPFINALIZER_H
