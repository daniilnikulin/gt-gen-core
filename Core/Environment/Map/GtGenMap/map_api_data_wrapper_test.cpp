/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/GtGenMap/map_api_data_wrapper.h"

#include <gtest/gtest.h>
#include <units.h>

#include <tuple>

namespace gtgen::core::environment::map
{

using namespace map_api;

class MapApiDataWrapperFixture : public testing::Test
{
  protected:
    template <class T>
    static void ExpectEqual(const T& right, const T& left)
    {
        EXPECT_EQ(right.id, left.id);
    }

    std::tuple<ReferenceLine, Lane, LaneBoundary, LogicalLane, LogicalLaneBoundary> AddNewElements(
        mantle_api::UniqueId id)
    {
        ReferenceLine reference_line{};
        Lane lane{};
        LaneBoundary lane_boundary{};
        LogicalLane logical_lane{};
        LogicalLaneBoundary logical_lane_boundary{};

        reference_line.id = id;
        lane.id = id;
        lane_boundary.id = id;
        logical_lane.id = id;
        logical_lane_boundary.id = id;

        map_api_data_.Add(reference_line);
        map_api_data_.Add(lane);
        map_api_data_.Add(lane_boundary);
        map_api_data_.Add(logical_lane);
        map_api_data_.Add(logical_lane_boundary);
        return {reference_line, lane, lane_boundary, logical_lane, logical_lane_boundary};
    }

    MapApiDataWrapper map_api_data_;
};

TEST_F(MapApiDataWrapperFixture, GivenEmptyData_WhenGettingAnyType_ThenExceptionIsThrown)
{
    ASSERT_EQ(map_api_data_.GetAll<ReferenceLine>().size(), 0);
    ASSERT_EQ(map_api_data_.GetAll<Lane>().size(), 0);
    ASSERT_EQ(map_api_data_.GetAll<LaneBoundary>().size(), 0);
    ASSERT_EQ(map_api_data_.GetAll<LogicalLane>().size(), 0);
    ASSERT_EQ(map_api_data_.GetAll<LogicalLaneBoundary>().size(), 0);
    EXPECT_THROW(map_api_data_.Get<ReferenceLine>(0), exception::MapException);
    EXPECT_THROW(map_api_data_.Get<Lane>(0), exception::MapException);
    EXPECT_THROW(map_api_data_.Get<LaneBoundary>(0), exception::MapException);
    EXPECT_THROW(map_api_data_.Get<LogicalLane>(0), exception::MapException);
    EXPECT_THROW(map_api_data_.Get<LogicalLaneBoundary>(0), exception::MapException);
}

TEST_F(MapApiDataWrapperFixture, GivenEmptyData_WhenFind_ThenNullptrIsReturned)
{
    EXPECT_EQ(map_api_data_.Find<ReferenceLine>(0), nullptr);
    EXPECT_EQ(map_api_data_.Find<Lane>(0), nullptr);
    EXPECT_EQ(map_api_data_.Find<LaneBoundary>(0), nullptr);
    EXPECT_EQ(map_api_data_.Find<LogicalLane>(0), nullptr);
    EXPECT_EQ(map_api_data_.Find<LogicalLaneBoundary>(0), nullptr);
}

TEST_F(MapApiDataWrapperFixture, GivenEmptyData_WhenGet_ThenExceptionIsThrown)
{
    EXPECT_THROW(map_api_data_.Get<ReferenceLine>(0), exception::MapElementNotFound);
    EXPECT_THROW(map_api_data_.Get<Lane>(0), exception::MapElementNotFound);
    EXPECT_THROW(map_api_data_.Get<LaneBoundary>(0), exception::MapElementNotFound);
    EXPECT_THROW(map_api_data_.Get<LogicalLane>(0), exception::MapElementNotFound);
    EXPECT_THROW(map_api_data_.Get<LogicalLaneBoundary>(0), exception::MapElementNotFound);
}

TEST_F(MapApiDataWrapperFixture, GivenDataWithSingleElement_WhenFindOrGet_ThenTheElementIsReturned)
{
    mantle_api::UniqueId expected_id{69};
    auto [reference_line, lane, lane_boundary, logical_lane, logical_lane_boundary] = AddNewElements(expected_id);

    auto* found_reference_line = map_api_data_.Find<ReferenceLine>(expected_id);
    auto* found_lane = map_api_data_.Find<Lane>(expected_id);
    auto* found_lane_boundary = map_api_data_.Find<LaneBoundary>(expected_id);
    auto* found_logical_lane = map_api_data_.Find<LogicalLane>(expected_id);
    auto* found_logical_lane_boundary = map_api_data_.Find<LogicalLaneBoundary>(expected_id);

    ASSERT_NE(found_reference_line, nullptr);
    ASSERT_NE(found_lane, nullptr);
    ASSERT_NE(found_lane_boundary, nullptr);
    ASSERT_NE(found_logical_lane, nullptr);
    ASSERT_NE(found_logical_lane_boundary, nullptr);
    ExpectEqual(*found_reference_line, reference_line);
    ExpectEqual(*found_lane, lane);
    ExpectEqual(*found_lane_boundary, lane_boundary);
    ExpectEqual(*found_logical_lane, logical_lane);
    ExpectEqual(*found_logical_lane_boundary, logical_lane_boundary);

    auto get_reference_line = map_api_data_.Get<ReferenceLine>(expected_id);
    auto get_lane = map_api_data_.Get<Lane>(expected_id);
    auto get_lane_boundary = map_api_data_.Get<LaneBoundary>(expected_id);
    auto get_logical_lane = map_api_data_.Get<LogicalLane>(expected_id);
    auto get_logical_lane_boundary = map_api_data_.Get<LogicalLaneBoundary>(expected_id);

    ExpectEqual(get_reference_line, reference_line);
    ExpectEqual(get_lane, lane);
    ExpectEqual(get_lane_boundary, lane_boundary);
    ExpectEqual(get_logical_lane, logical_lane);
    ExpectEqual(get_logical_lane_boundary, logical_lane_boundary);
}

TEST_F(MapApiDataWrapperFixture, GivenDataWithMultipleElements_WhenFindOrGetNonExistingElement_ThenExceptionIsThrown)
{
    AddNewElements(6);
    AddNewElements(69);
    AddNewElements(3);

    EXPECT_THROW(map_api_data_.Get<ReferenceLine>(80), exception::MapElementNotFound);
    EXPECT_THROW(map_api_data_.Get<Lane>(77), exception::MapElementNotFound);
    EXPECT_THROW(map_api_data_.Get<LaneBoundary>(15), exception::MapElementNotFound);
    EXPECT_THROW(map_api_data_.Get<LogicalLane>(94), exception::MapElementNotFound);
    EXPECT_THROW(map_api_data_.Get<LogicalLaneBoundary>(16), exception::MapElementNotFound);
}

TEST_F(MapApiDataWrapperFixture, GivenDataWithMultipleElements_WhenFindOrGet_ThenCorrectElementIsReturned)
{
    auto [reference_line6, lane6, lane_boundary6, logical_lane6, logical_lane_boundary6] = AddNewElements(6);
    auto [reference_line69, lane69, lane_boundary69, logical_lane69, logical_lane_boundary69] = AddNewElements(69);
    auto [reference_line3, lane3, lane_boundary3, logical_lane3, logical_lane_boundary3] = AddNewElements(3);
    auto [reference_line81, lane81, lane_boundary81, logical_lane81, logical_lane_boundary81] = AddNewElements(81);
    auto [reference_line77, lane77, lane_boundary77, logical_lane77, logical_lane_boundary77] = AddNewElements(77);

    auto get_reference_line6 = map_api_data_.Get<ReferenceLine>(6);
    auto get_lane77 = map_api_data_.Get<Lane>(77);
    auto get_lane_boundary81 = map_api_data_.Get<LaneBoundary>(81);
    auto get_logical_lane69 = map_api_data_.Get<LogicalLane>(69);
    auto get_logical_lane_boundary3 = map_api_data_.Get<LogicalLaneBoundary>(3);

    EXPECT_EQ(get_reference_line6.id, 6);
    EXPECT_EQ(get_lane77.id, 77);
    EXPECT_EQ(get_lane_boundary81.id, 81);
    EXPECT_EQ(get_logical_lane69.id, 69);
    EXPECT_EQ(get_logical_lane_boundary3.id, 3);
}

TEST_F(MapApiDataWrapperFixture, GivenMapWithAllPossibleElements_WhenClearMap_ThenAllElementsArePurged)
{
    AddNewElements(6);
    AddNewElements(69);
    AddNewElements(3);

    EXPECT_EQ(map_api_data_.GetAll<ReferenceLine>().size(), 3);
    EXPECT_EQ(map_api_data_.GetAll<Lane>().size(), 3);
    EXPECT_EQ(map_api_data_.GetAll<LaneBoundary>().size(), 3);
    EXPECT_EQ(map_api_data_.GetAll<LogicalLane>().size(), 3);
    EXPECT_EQ(map_api_data_.GetAll<LogicalLaneBoundary>().size(), 3);

    map_api_data_.ClearMap();

    EXPECT_EQ(map_api_data_.GetAll<ReferenceLine>().size(), 0);
    EXPECT_EQ(map_api_data_.GetAll<Lane>().size(), 0);
    EXPECT_EQ(map_api_data_.GetAll<LaneBoundary>().size(), 0);
    EXPECT_EQ(map_api_data_.GetAll<LogicalLane>().size(), 0);
    EXPECT_EQ(map_api_data_.GetAll<LogicalLaneBoundary>().size(), 0);
}

TEST_F(MapApiDataWrapperFixture, GivenEmptyData_WhenRefreshReferencesForLogicalLane_ThenExceptionIsThrown)
{
    map_api::LogicalLane tested_logical_lane;
    tested_logical_lane.id = 1;

    EXPECT_THROW(
        {
            try
            {
                map_api_data_.RefreshReferencesForLogicalLane(tested_logical_lane);
            }
            catch (const exception::MapElementNotFound& e)
            {
                std::string expected_message =
                    "No corresponding logical lane added internally, could not refresh LogicalLane with ID " +
                    std::to_string(tested_logical_lane.id);
                EXPECT_EQ(e.what(), expected_message);
                throw;
            }
        },
        exception::MapElementNotFound);
}

TEST_F(MapApiDataWrapperFixture,
       GivenDataWithMultipleValidElements_WhenRefreshReferencesForLogicalLane_ThenLogicalLaneIsRefreshed)
{
    // create input logical lane for the refresh function
    map_api::LogicalLane tested_logical_lane{};
    tested_logical_lane.id = 1;
    map_api::ReferenceLine reference_line{};
    reference_line.id = 2;
    tested_logical_lane.reference_line = &reference_line;
    map_api::LogicalLane left_adjacent_logical_lane{};
    left_adjacent_logical_lane.id = 3;
    map_api::LogicalLaneRelation left_adjacent_lane_relation{left_adjacent_logical_lane,
                                                             units::length::meter_t(0),
                                                             units::length::meter_t(0),
                                                             units::length::meter_t(0),
                                                             units::length::meter_t(0)};
    tested_logical_lane.left_adjacent_lanes.push_back(left_adjacent_lane_relation);
    map_api::Lane physical_lane{};
    physical_lane.id = 4;
    map_api::PhysicalLaneReference physical_lane_reference{
        physical_lane, units::length::meter_t(0), units::length::meter_t(0)};
    tested_logical_lane.physical_lane_references.push_back(physical_lane_reference);
    map_api::LogicalLaneBoundary left_logical_lane_boundary{};
    left_logical_lane_boundary.id = 5;
    left_logical_lane_boundary.reference_line = &reference_line;
    tested_logical_lane.left_boundaries.push_back(std::ref(left_logical_lane_boundary));

    // create an internal logical lane in data wrapper
    map_api::LogicalLane internal_logical_lane{};
    internal_logical_lane.id = 1;
    map_api::ReferenceLine internal_reference_line{};
    internal_reference_line.id = 2;
    map_api::LogicalLane internal_left_adjacent_logical_lane{};
    internal_left_adjacent_logical_lane.id = 3;
    map_api::Lane internal_physical_lane{};
    internal_physical_lane.id = 4;
    map_api::LogicalLaneBoundary internal_left_logical_lane_boundary{};
    internal_left_logical_lane_boundary.id = 5;
    internal_logical_lane.left_boundaries.push_back(std::ref(internal_left_logical_lane_boundary));

    map_api_data_.Add(internal_logical_lane);
    map_api_data_.Add(internal_reference_line);
    map_api_data_.Add(internal_left_adjacent_logical_lane);
    map_api_data_.Add(internal_physical_lane);
    map_api_data_.Add(internal_left_logical_lane_boundary);

    map_api_data_.RefreshReferencesForLogicalLane(tested_logical_lane);

    const auto& refreshed_logical_lane = map_api_data_.Get<map_api::LogicalLane>(1);

    EXPECT_EQ(refreshed_logical_lane.reference_line->id, 2);
    ASSERT_EQ(refreshed_logical_lane.left_adjacent_lanes.size(), 1);
    EXPECT_EQ(refreshed_logical_lane.left_adjacent_lanes.at(0).other_lane.id, 3);
    ASSERT_EQ(refreshed_logical_lane.physical_lane_references.size(), 1);
    EXPECT_EQ(refreshed_logical_lane.physical_lane_references.at(0).physical_lane.id, 4);
    ASSERT_EQ(refreshed_logical_lane.left_boundaries.size(), 1);
    EXPECT_EQ(refreshed_logical_lane.left_boundaries.at(0).get().id, 5);
}

}  // namespace gtgen::core::environment::map
