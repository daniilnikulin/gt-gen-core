/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/road_object_types_converter.h"

#include <gmock/gmock.h>

namespace gtgen::core::environment::map::map
{
TEST(ConvertRoadObjectTypeTest, GivenRoadObjectType_WhenConvert_ThenMantleAPIRoadObjectTypeIsCorrect)
{
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kBarrier), mantle_api::StaticObjectType::kBarrier);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kBridge), mantle_api::StaticObjectType::kBridge);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kBuilding),
              mantle_api::StaticObjectType::kBuilding);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kConstructionSiteElement),
              mantle_api::StaticObjectType::kConstructionSiteElement);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kCurbstone),
              mantle_api::StaticObjectType::kCurbstone);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kDelineator),
              mantle_api::StaticObjectType::kDelineator);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kEmittingStructure),
              mantle_api::StaticObjectType::kEmittingStructure);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kOverheadStructure),
              mantle_api::StaticObjectType::kOverheadStructure);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kRectangularStructure),
              mantle_api::StaticObjectType::kRectangularStructure);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kReflectiveStructure),
              mantle_api::StaticObjectType::kReflectiveStructure);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kVerticalStructure),
              mantle_api::StaticObjectType::kVerticalStructure);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kPole), mantle_api::StaticObjectType::kPole);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kPylon), mantle_api::StaticObjectType::kPylon);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kSpeedBump),
              mantle_api::StaticObjectType::kSpeedBump);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kTree), mantle_api::StaticObjectType::kTree);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kVegetation),
              mantle_api::StaticObjectType::kVegetation);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kWall), mantle_api::StaticObjectType::kWall);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kUnknown), mantle_api::StaticObjectType::kInvalid);
    EXPECT_EQ(ConvertRoadObjectType(map_api::StationaryObject::Type::kOther), mantle_api::StaticObjectType::kOther);
}

TEST(ConvertRoadObjectTypeTest,
     GivenUnsupportedRoadObjectType_WhenConvert_ThenMantleAPIRoadObjectTypeIsOtherWithWarningMessage)
{
    testing::internal::CaptureStdout();
    EXPECT_EQ(ConvertRoadObjectType(static_cast<map_api::StationaryObject::Type>(100)),
              mantle_api::StaticObjectType::kInvalid);
    EXPECT_THAT(
        testing::internal::GetCapturedStdout(),
        testing::HasSubstr(
            "The given map_api::StationaryObject::Type value=100 cannot be converted! Return Invalid as default."));
}

TEST(ConvertRoadObjectMaterialTest, GivenMaterial_WhenConvert_ThenOsiStationaryObjectEntityMaterialIsCorrect)
{
    EXPECT_EQ(ConvertRoadObjectMaterial(map_api::StationaryObject::Material::kUnknown),
              osi::StationaryObjectEntityMaterial::kUnknown);
    EXPECT_EQ(ConvertRoadObjectMaterial(map_api::StationaryObject::Material::kOther),
              osi::StationaryObjectEntityMaterial::kOther);
    EXPECT_EQ(ConvertRoadObjectMaterial(map_api::StationaryObject::Material::kWood),
              osi::StationaryObjectEntityMaterial::kWood);
    EXPECT_EQ(ConvertRoadObjectMaterial(map_api::StationaryObject::Material::kPlastic),
              osi::StationaryObjectEntityMaterial::kPlastic);
    EXPECT_EQ(ConvertRoadObjectMaterial(map_api::StationaryObject::Material::kConcrete),
              osi::StationaryObjectEntityMaterial::kConcrete);
    EXPECT_EQ(ConvertRoadObjectMaterial(map_api::StationaryObject::Material::kMetal),
              osi::StationaryObjectEntityMaterial::kMetal);
    EXPECT_EQ(ConvertRoadObjectMaterial(map_api::StationaryObject::Material::kStone),
              osi::StationaryObjectEntityMaterial::kStone);
    EXPECT_EQ(ConvertRoadObjectMaterial(map_api::StationaryObject::Material::kGlas),
              osi::StationaryObjectEntityMaterial::kGlas);
    EXPECT_EQ(ConvertRoadObjectMaterial(map_api::StationaryObject::Material::kMud),
              osi::StationaryObjectEntityMaterial::kMud);
}

TEST(ConvertRoadObjectMaterialTest,
     GivenUnsupportedMaterial_WhenConvert_ThenOsiStationaryObjectEntityMaterialIsUnknownWithWarningMessage)
{
    testing::internal::CaptureStdout();
    EXPECT_EQ(ConvertRoadObjectMaterial(static_cast<map_api::StationaryObject::Material>(100)),
              osi::StationaryObjectEntityMaterial::kUnknown);
    EXPECT_THAT(
        testing::internal::GetCapturedStdout(),
        testing::HasSubstr(
            "The given map_api::StationaryObject::Material value=100 cannot be converted! Return Other as default."));
}

}  // namespace gtgen::core::environment::map::map
