/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/mapapi_to_gtgenmap_converter_impl.h"

#include <gmock/gmock.h>

namespace gtgen::core::environment::map::map
{
using units::literals::operator""_m;

inline auto CreateMapApiLaneBoundary(map_api::Identifier id,
                                     const units::length::meter_t start_x,
                                     const units::length::meter_t y_offset)
{
    auto lane_boundary = std::make_unique<map_api::LaneBoundary>();
    lane_boundary->id = id;
    lane_boundary->boundary_line = std::vector<map_api::LaneBoundary::BoundaryPoint>{
        map_api::LaneBoundary::BoundaryPoint{map_api::Position3d{start_x, y_offset, 0.0_m},
                                             0.13_m,
                                             0.05_m,
                                             map_api::LaneBoundary::BoundaryPoint::Dash{}},
        map_api::LaneBoundary::BoundaryPoint{map_api::Position3d{start_x + 1000.0_m, y_offset, 0.0_m},
                                             0.13_m,
                                             0.05_m,
                                             map_api::LaneBoundary::BoundaryPoint::Dash{}}};
    lane_boundary->type = map_api::LaneBoundary::Type::kSolidLine;
    lane_boundary->color = map_api::LaneBoundary::Color::kWhite;
    return lane_boundary;
}

inline auto CreateMapApiLane(map_api::Identifier id,
                             const units::length::meter_t start_x,
                             const units::length::meter_t y_offset)
{
    auto lane = std::make_unique<map_api::Lane>();
    lane->id = id;
    lane->type = map_api::Lane::Type::kDriving;
    lane->sub_type = map_api::Lane::Subtype::kUnknown;
    lane->centerline = map_api::Lane::Polyline{map_api::Lane::PolylinePoint{start_x, y_offset, 0.0_m},
                                               map_api::Lane::PolylinePoint{start_x + 1000.0_m, y_offset, 0.0_m}};

    return lane;
}

inline auto CreateReferenceLine(const map_api::Identifier id,
                                const std::size_t number_of_points,
                                const map_api::Position3d start_point,
                                const units::angle::radian_t angle,
                                const units::length::meter_t step_length)
{
    map_api::ReferenceLine reference_line{};
    reference_line.id = id;
    reference_line.poly_line.reserve(number_of_points);
    const units::angle::radian_t half_pi{std::acos(0.0)};
    const units::angle::radian_t t_axis_yaw{angle + half_pi};
    const units::length::meter_t step_length_x{step_length * units::math::cos(angle)};
    const units::length::meter_t step_length_y{step_length * units::math::sin(angle)};
    for (std::size_t i = 0; i < number_of_points; ++i)
    {
        map_api::ReferenceLinePoint point{};
        point.s_position = step_length * i;
        point.t_axis_yaw = t_axis_yaw;
        point.world_position.x = start_point.x + step_length_x * i;
        point.world_position.y = start_point.y + step_length_y * i;
        point.world_position.z = start_point.z;
        reference_line.poly_line.push_back(point);
    }
    return reference_line;
}

inline auto CreateLogicalLaneBoundary(const map_api::Identifier id,
                                      const units::length::meter_t start_x,
                                      const units::length::meter_t start_y,
                                      const units::length::meter_t t_offset)
{
    map_api::LogicalLaneBoundary logical_lane_boundary{};

    logical_lane_boundary.id = id;
    logical_lane_boundary.boundary_line.emplace_back(
        map_api::LogicalBoundaryPoint{map_api::Position3d{start_x, start_y, 0_m}, 0_m, t_offset});
    logical_lane_boundary.boundary_line.push_back(
        map_api::LogicalBoundaryPoint{map_api::Position3d{start_x + 1000_m, start_y, 0_m}, 1000_m, t_offset});
    logical_lane_boundary.passing_rule = map_api::LogicalLaneBoundary::PassingRule::kUnknown;

    return logical_lane_boundary;
}

inline auto CreateLogicalLane(const map_api::Identifier id)
{
    map_api::LogicalLane logical_lane{};

    logical_lane.id = id;
    logical_lane.start_s = 0_m;
    logical_lane.end_s = 1000_m;
    logical_lane.type = map_api::LogicalLane::Type::kUnknown;
    logical_lane.move_direction = map_api::LogicalLane::MoveDirection::kUnknown;

    return logical_lane;
}

///////////////////////////////////////////////////////////
/// @verbatim
/// Connected Lanes
/// IDs:
/// |-----------------------------|-----------------------------| y = 4.5
/// |    Left LaneBoundary:  100  |    Left LaneBoundary:  103  |
/// |    Lane: 0                  |    Lane: 2                  | y = 3.0
/// |    Right LaneBoundary: 101  |    Right LaneBoundary: 104  |
/// |-----------------------------|-----------------------------| y = 1.5
/// |    Left LaneBoundary:  101  |    Left LaneBoundary:  104  |
/// |    Lane: 1                  |    Lane: 3                  | y = 0.0
/// |    Right LaneBoundary: 102  |    Right LaneBoundary: 105  |
/// |-----------------------------|-----------------------------| y = -1.5
/// ^                             ^                             ^
/// x(0)                         (1000)                       (2000)
///
/// @endverbatim
inline map_api::Map CreateMapWithConnectedLanes()
{
    map_api::Map map{};

    map.projection_string = "+proj=utm +zone=32 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs";

    auto lane_boundary_100 = CreateMapApiLaneBoundary(100, 0.0_m, 4.5_m);
    auto lane_boundary_101 = CreateMapApiLaneBoundary(101, 0.0_m, 1.5_m);
    auto lane_boundary_102 = CreateMapApiLaneBoundary(102, 0.0_m, -1.5_m);
    auto lane_boundary_103 = CreateMapApiLaneBoundary(103, 1000.0_m, 4.5_m);
    auto lane_boundary_104 = CreateMapApiLaneBoundary(104, 1000.0_m, 1.5_m);
    auto lane_boundary_105 = CreateMapApiLaneBoundary(105, 1000.0_m, -1.5_m);

    auto lane_0 = CreateMapApiLane(0, 0.0_m, 3.0_m);
    auto lane_1 = CreateMapApiLane(1, 0.0_m, 3.0_m);
    auto lane_2 = CreateMapApiLane(2, 1000.0_m, 3.0_m);
    auto lane_3 = CreateMapApiLane(3, 1000.0_m, 0.0_m);

    lane_0->left_lane_boundaries.push_back(*lane_boundary_100);
    lane_0->right_lane_boundaries.push_back(*lane_boundary_101);
    lane_0->right_adjacent_lanes.push_back(*lane_1);
    lane_0->successor_lanes.push_back(*lane_2);

    lane_1->left_lane_boundaries.push_back(*lane_boundary_101);
    lane_1->right_lane_boundaries.push_back(*lane_boundary_102);
    lane_1->left_adjacent_lanes.push_back(*lane_0);
    lane_1->successor_lanes.push_back(*lane_3);

    lane_2->left_lane_boundaries.push_back(*lane_boundary_103);
    lane_2->right_lane_boundaries.push_back(*lane_boundary_104);
    lane_2->right_adjacent_lanes.push_back(*lane_3);
    lane_2->antecessor_lanes.push_back(*lane_0);

    lane_3->left_lane_boundaries.push_back(*lane_boundary_104);
    lane_3->right_lane_boundaries.push_back(*lane_boundary_105);
    lane_3->left_adjacent_lanes.push_back(*lane_2);
    lane_3->antecessor_lanes.push_back(*lane_1);

    auto lane_group_1 = std::make_unique<map_api::LaneGroup>();
    lane_group_1->id = 1;
    lane_group_1->type = map_api::LaneGroup::Type::kOneWay;
    lane_group_1->lanes.push_back(*lane_0);
    lane_group_1->lanes.push_back(*lane_1);
    lane_group_1->lane_boundaries.push_back(*lane_boundary_100);
    lane_group_1->lane_boundaries.push_back(*lane_boundary_101);
    lane_group_1->lane_boundaries.push_back(*lane_boundary_102);
    map.lane_groups.push_back(std::move(lane_group_1));

    auto lane_group_2 = std::make_unique<map_api::LaneGroup>();
    lane_group_2->id = 2;
    lane_group_2->type = map_api::LaneGroup::Type::kOther;
    lane_group_2->lanes.push_back(*lane_2);
    lane_group_2->lanes.push_back(*lane_3);
    lane_group_2->lane_boundaries.push_back(*lane_boundary_103);
    lane_group_2->lane_boundaries.push_back(*lane_boundary_104);
    lane_group_2->lane_boundaries.push_back(*lane_boundary_105);
    map.lane_groups.push_back(std::move(lane_group_2));

    map.lanes.emplace_back(std::move(lane_0));
    map.lanes.emplace_back(std::move(lane_1));
    map.lanes.emplace_back(std::move(lane_2));
    map.lanes.emplace_back(std::move(lane_3));
    map.lane_boundaries.emplace_back(std::move(lane_boundary_100));
    map.lane_boundaries.emplace_back(std::move(lane_boundary_101));
    map.lane_boundaries.emplace_back(std::move(lane_boundary_102));
    map.lane_boundaries.emplace_back(std::move(lane_boundary_103));
    map.lane_boundaries.emplace_back(std::move(lane_boundary_104));
    map.lane_boundaries.emplace_back(std::move(lane_boundary_105));

    auto traffic_sign = std::make_unique<map_api::TrafficSign>();
    traffic_sign->id = 42;
    traffic_sign->main_sign.type = map_api::MainSignType::kSpeedLimitBegin;
    traffic_sign->supplementary_signs.emplace_back(map_api::SupplementarySign{});
    traffic_sign->supplementary_signs.back().type = map_api::SupplementarySignType::kRain;
    traffic_sign->supplementary_signs.emplace_back(map_api::SupplementarySign{});
    traffic_sign->supplementary_signs.back().type = map_api::SupplementarySignType::kSnow;

    auto traffic_light = std::make_unique<map_api::TrafficLight>();
    traffic_light->id = 41;
    traffic_light->traffic_signals.emplace_back(map_api::TrafficLight::TrafficSignal{});
    traffic_light->traffic_signals.back().id = 201;
    traffic_light->traffic_signals.back().color = map_api::TrafficLight::TrafficSignal::Color::kGreen;
    traffic_light->traffic_signals.back().mode = map_api::TrafficLight::TrafficSignal::Mode::kOff;
    traffic_light->traffic_signals.emplace_back(map_api::TrafficLight::TrafficSignal{});
    traffic_light->traffic_signals.back().id = 202;
    traffic_light->traffic_signals.back().color = map_api::TrafficLight::TrafficSignal::Color::kRed;
    traffic_light->traffic_signals.back().mode = map_api::TrafficLight::TrafficSignal::Mode::kConstant;

    auto stationary_object = std::make_unique<map_api::StationaryObject>();
    stationary_object->id = 50;
    stationary_object->type = map_api::StationaryObject::Type::kPole;

    map.traffic_signs.emplace_back(std::move(traffic_sign));
    map.traffic_lights.emplace_back(std::move(traffic_light));
    map.stationary_objects.emplace_back(std::move(stationary_object));

    {
        auto road_marking = std::make_unique<map_api::RoadMarking>();
        road_marking->id = 40;
        road_marking->monochrome_color = map_api::RoadMarking::Color::kRed;
        road_marking->type = map_api::RoadMarking::Type::kPaintedTrafficSign;
        road_marking->traffic_main_sign_type = map_api::MainSignType::kZebraCrossing;

        map_api::TrafficSignValue sign_value;
        sign_value.value = 123.45;
        sign_value.value_unit = map_api::TrafficSignValue::Unit::kKilometerPerHour;
        sign_value.text = "Speed Limit 50";
        road_marking->value = sign_value;

        map.road_markings.emplace_back(std::move(road_marking));
    }

    map.lane_boundaries.front()->limiting_structures.push_back(*(map.stationary_objects.front()));

    // Marienplatz position
    const map_api::Position3d start_position{691612.58_m, 5334764.11_m, 519_m};

    for (const auto& [id, angle] : std::vector<std::pair<std::size_t, double>>{{402, -0.78}, {4242, 0.78}})
    {
        map.reference_lines.emplace_back(std::make_unique<map_api::ReferenceLine>(
            CreateReferenceLine(id, 10, start_position, units::angle::radian_t{angle}, units::length::meter_t{13})));
    }

    map.logical_lane_boundaries.emplace_back(
        std::make_unique<map_api::LogicalLaneBoundary>(CreateLogicalLaneBoundary(1000, 0_m, 4.5_m, 3_m)));
    map.logical_lane_boundaries.emplace_back(
        std::make_unique<map_api::LogicalLaneBoundary>(CreateLogicalLaneBoundary(1001, 0_m, 1.5_m, 1.5_m)));

    map.logical_lanes.emplace_back(std::make_unique<map_api::LogicalLane>(CreateLogicalLane(1002)));
    map.logical_lanes.emplace_back(std::make_unique<map_api::LogicalLane>(CreateLogicalLane(1003)));

    return map;
}

class MapApiToGtGenMapConverterImplTest : public testing::Test
{
  public:
    void SetUp() override
    {
        gtgen_map_.SetSourceMapType(GtGenMap::SourceMapType::kOpenDrive);
        converter_ = std::make_unique<MapApiToGtGenMapConverterImpl>(unique_id_provider_, osi_map_data_, gtgen_map_);
    }

  protected:
    GtGenMap gtgen_map_;
    service::utility::UniqueIdProvider unique_id_provider_{};
    map_api::Map osi_map_data_{CreateMapWithConnectedLanes()};
    std::unique_ptr<MapApiToGtGenMapConverterImpl> converter_;
};

TEST_F(MapApiToGtGenMapConverterImplTest, GivenGtGenMapThatSourceTypeIsNotSet_WhenConvert_ThenThrow)
{
    // Arrange
    map_api::Map data{};
    GtGenMap gtgen_map{};
    MapApiToGtGenMapConverterImpl converter{unique_id_provider_, data, gtgen_map};

    // Act & Assert
    EXPECT_ANY_THROW(converter.Convert());
}

TEST_F(MapApiToGtGenMapConverterImplTest, GivenEmptyMap_WhenConvertToGtGenMap_ThenNoThrow)
{
    // Arrange
    map_api::Map data{};
    MapApiToGtGenMapConverterImpl converter{unique_id_provider_, data, gtgen_map_};

    // Act & Assert
    EXPECT_NO_THROW(converter.Convert());
}

TEST_F(MapApiToGtGenMapConverterImplTest, GivenMapWithProjectString_WhenConvertToGtGenMap_ThenProjectStringIsConverted)
{
    // Arrange
    map_api::Map data{};
    data.projection_string =
        "proj=tmerc +lat_0=0 +lon_0=9 +k=0.9996 +x_0=-194000 +y_0=-5346000 +datum=WGS84 +units=m +no_defs";

    MapApiToGtGenMapConverterImpl converter{unique_id_provider_, data, gtgen_map_};

    // Act
    converter.Convert();

    // Assert
    EXPECT_EQ(gtgen_map_.projection_string, data.projection_string);
}

TEST_F(MapApiToGtGenMapConverterImplTest, GivenEmptyMap_WhenConvertToGtGenMap_ThenNoLaneGroupIsCreated)
{
    // Arrange
    map_api::Map data{};
    MapApiToGtGenMapConverterImpl converter{unique_id_provider_, data, gtgen_map_};

    // Act
    converter.Convert();

    // Assert
    ASSERT_EQ(gtgen_map_.GetLaneGroups().size(), 0);
}

TEST_F(MapApiToGtGenMapConverterImplTest, GivenMapApiWithLaneGroups_WhenConvertToGtGenMap_ThenLaneGroupsAreConverted)
{
    // Arrange
    const mantle_api::UniqueId expected_lane_group_1_id{1};
    const mantle_api::UniqueId expected_lane_group_2_id{2};

    // Act
    converter_->Convert();

    // Assert
    ASSERT_EQ(gtgen_map_.GetLaneGroups().size(), 2);
    ASSERT_TRUE(gtgen_map_.ContainsLaneGroup(expected_lane_group_1_id));
    const auto lane_group_1 = gtgen_map_.GetLaneGroup(expected_lane_group_1_id);
    EXPECT_EQ(lane_group_1.type, LaneGroup::Type::kOneWay);
    EXPECT_THAT(lane_group_1.lane_ids, ::testing::ElementsAre(0, 1));
    EXPECT_THAT(lane_group_1.lane_boundary_ids, ::testing::ElementsAre(100, 101, 102));

    ASSERT_TRUE(gtgen_map_.ContainsLaneGroup(expected_lane_group_2_id));
    const auto lane_group_2 = gtgen_map_.GetLaneGroup(expected_lane_group_2_id);
    EXPECT_EQ(lane_group_2.type, LaneGroup::Type::kOther);
    EXPECT_THAT(lane_group_2.lane_ids, ::testing::ElementsAre(2, 3));
    EXPECT_THAT(lane_group_2.lane_boundary_ids, ::testing::ElementsAre(103, 104, 105));
}

TEST_F(MapApiToGtGenMapConverterImplTest,
       GivenMapApiWithLaneBoundaries_WhenConvertToGtGenMap_ThenBoundariesAreConverted)
{
    // Arrange
    const std::vector<map_api::Identifier> expect_lane_boundary_ids{100, 101, 102, 103, 104, 105};

    // Act
    converter_->Convert();

    // Assert
    ASSERT_EQ(gtgen_map_.GetLaneBoundaries().size(), 6);
    for (const auto& lane_boundary : gtgen_map_.GetLaneBoundaries())
    {
        EXPECT_THAT(expect_lane_boundary_ids, ::testing::Contains(lane_boundary.id));
    }
}

TEST_F(MapApiToGtGenMapConverterImplTest, GivenMapApiWithLanes_WhenConvertToGtGenMap_ThenLanesAreConvertedCorrectly)
{
    // Arrange
    const std::vector<map_api::Identifier> expect_lane_ids{0, 1, 2, 3};

    // Act
    converter_->Convert();

    // Assert
    ASSERT_EQ(gtgen_map_.GetLanes().size(), 4);

    const auto lane_0 = gtgen_map_.GetLane(0);
    EXPECT_EQ(lane_0.left_lane_boundaries, std::vector<mantle_api::UniqueId>{100});
    EXPECT_EQ(lane_0.right_lane_boundaries, std::vector<mantle_api::UniqueId>{101});
    EXPECT_EQ(lane_0.successors, std::vector<mantle_api::UniqueId>{2});
    EXPECT_EQ(lane_0.right_adjacent_lanes, std::vector<mantle_api::UniqueId>{1});

    const auto lane_1 = gtgen_map_.GetLane(1);
    EXPECT_EQ(lane_1.left_lane_boundaries, std::vector<mantle_api::UniqueId>{101});
    EXPECT_EQ(lane_1.right_lane_boundaries, std::vector<mantle_api::UniqueId>{102});
    EXPECT_EQ(lane_1.left_adjacent_lanes, std::vector<mantle_api::UniqueId>{0});
    EXPECT_EQ(lane_1.successors, std::vector<mantle_api::UniqueId>{3});

    const auto lane_2 = gtgen_map_.GetLane(2);
    EXPECT_EQ(lane_2.left_lane_boundaries, std::vector<mantle_api::UniqueId>{103});
    EXPECT_EQ(lane_2.right_lane_boundaries, std::vector<mantle_api::UniqueId>{104});
    EXPECT_EQ(lane_2.right_adjacent_lanes, std::vector<mantle_api::UniqueId>{3});
    EXPECT_EQ(lane_2.predecessors, std::vector<mantle_api::UniqueId>{0});

    const auto lane_3 = gtgen_map_.GetLane(3);
    EXPECT_EQ(lane_3.left_lane_boundaries, std::vector<mantle_api::UniqueId>{104});
    EXPECT_EQ(lane_3.right_lane_boundaries, std::vector<mantle_api::UniqueId>{105});
    EXPECT_EQ(lane_3.left_adjacent_lanes, std::vector<mantle_api::UniqueId>{2});
    EXPECT_EQ(lane_3.predecessors, std::vector<mantle_api::UniqueId>{1});
}

TEST_F(MapApiToGtGenMapConverterImplTest,
       GivenMapApiWithRoadMarkings_WhenConvertToGtGenMap_ThenRoadMarkingsAreConverted)
{
    // Arrange in SetUp
    // Act
    converter_->Convert();

    // Assert
    ASSERT_EQ(gtgen_map_.traffic_signs.size(), 2);
    const auto* ground_sign = dynamic_cast<GroundSign*>(gtgen_map_.traffic_signs.at(1).get());
    ASSERT_TRUE(ground_sign);
    EXPECT_EQ(ground_sign->id, 40);
    EXPECT_EQ(ground_sign->type, osi::OsiTrafficSignType::kZebraCrossing);
    EXPECT_EQ(ground_sign->marking_color, osi::OsiRoadMarkingColor::kRed);
    EXPECT_EQ(ground_sign->marking_type, osi::OsiRoadMarkingsType::kPaintedTrafficSign);
    EXPECT_EQ(ground_sign->value_information.value, 123.45);
    EXPECT_EQ(ground_sign->value_information.value_unit, osi::OsiTrafficSignValueUnit::kKilometerPerHour);
    EXPECT_EQ(ground_sign->value_information.text, "Speed Limit 50");
}

TEST_F(MapApiToGtGenMapConverterImplTest,
       GivenMapApiWithTrafficSigns_WhenConvertToGtGenMap_ThenTrafficSignsAreConverted)
{
    // Arrange in SetUp
    // Act
    converter_->Convert();

    // Assert
    ASSERT_EQ(gtgen_map_.traffic_signs.size(), 2);
    const auto* mounted_sign = dynamic_cast<MountedSign*>(gtgen_map_.traffic_signs.at(0).get());
    ASSERT_TRUE(mounted_sign);
    EXPECT_EQ(mounted_sign->id, 42);
    EXPECT_EQ(mounted_sign->type, osi::OsiTrafficSignType::kSpeedLimitBegin);
    EXPECT_EQ(mounted_sign->supplementary_signs.size(), 2);
    EXPECT_EQ(mounted_sign->supplementary_signs.at(0).type, OsiSupplementarySignType::kRain);
    EXPECT_EQ(mounted_sign->supplementary_signs.at(1).type, OsiSupplementarySignType::kSnow);
}

TEST_F(MapApiToGtGenMapConverterImplTest,
       GivenMapApiWithTrafficLights_WhenConvertToGtGenMap_ThenTrafficLightsAreConverted)
{
    // Arrange in SetUp
    // Act
    converter_->Convert();

    // Assert
    ASSERT_EQ(gtgen_map_.traffic_lights.size(), 1);
    const auto traffic_light = gtgen_map_.traffic_lights.at(0);
    EXPECT_EQ(traffic_light.id, 41);

    ASSERT_EQ(traffic_light.light_bulbs.size(), 2);
    EXPECT_EQ(traffic_light.light_bulbs.at(0).id, 201);
    EXPECT_EQ(traffic_light.light_bulbs.at(1).id, 202);
}

TEST_F(MapApiToGtGenMapConverterImplTest,
       GivenMapApiWithReferenceLines_WhenConvertToGtGenMap_ThenReferenceLinesAreConverted)
{
    // Arrange in SetUp
    // Act
    converter_->Convert();

    // Assert
    ASSERT_EQ(gtgen_map_.GetAll<map_api::ReferenceLine>().size(), 2);
    EXPECT_EQ(gtgen_map_.GetAll<map_api::ReferenceLine>().at(0).id, 402);
    EXPECT_EQ(gtgen_map_.GetAll<map_api::ReferenceLine>().at(1).id, 4242);
    ASSERT_EQ(gtgen_map_.GetAll<map_api::ReferenceLine>().at(0).poly_line.size(), 10);
    ASSERT_EQ(gtgen_map_.GetAll<map_api::ReferenceLine>().at(1).poly_line.size(), 10);

    ASSERT_EQ(osi_map_data_.reference_lines.size(), 2);
    for (std::size_t i = 0; i < 10; ++i)
    {
        EXPECT_EQ(gtgen_map_.GetAll<map_api::ReferenceLine>().at(0).poly_line.at(i).world_position,
                  osi_map_data_.reference_lines.at(0)->poly_line.at(i).world_position);
        EXPECT_EQ(gtgen_map_.GetAll<map_api::ReferenceLine>().at(0).poly_line.at(i).s_position,
                  osi_map_data_.reference_lines.at(0)->poly_line.at(i).s_position);
        EXPECT_EQ(gtgen_map_.GetAll<map_api::ReferenceLine>().at(0).poly_line.at(i).t_axis_yaw,
                  osi_map_data_.reference_lines.at(0)->poly_line.at(i).t_axis_yaw);

        EXPECT_EQ(gtgen_map_.GetAll<map_api::ReferenceLine>().at(1).poly_line.at(i).world_position,
                  osi_map_data_.reference_lines.at(1)->poly_line.at(i).world_position);
        EXPECT_EQ(gtgen_map_.GetAll<map_api::ReferenceLine>().at(1).poly_line.at(i).s_position,
                  osi_map_data_.reference_lines.at(1)->poly_line.at(i).s_position);
        EXPECT_EQ(gtgen_map_.GetAll<map_api::ReferenceLine>().at(1).poly_line.at(i).t_axis_yaw,
                  osi_map_data_.reference_lines.at(1)->poly_line.at(i).t_axis_yaw);
    }
}

TEST_F(MapApiToGtGenMapConverterImplTest,
       GivenMapApiWithStationaryObject_WhenConvertToGtGenMap_ThenRoadObjectIsConverted)
{
    // Arrange in SetUp
    // Act
    converter_->Convert();

    // Assert
    ASSERT_EQ(gtgen_map_.road_objects.size(), 1);
    const auto road_object = gtgen_map_.road_objects[0];
    EXPECT_EQ(road_object.id, 50);
    EXPECT_EQ(road_object.type, mantle_api::StaticObjectType::kPole);
}

TEST_F(MapApiToGtGenMapConverterImplTest,
       GivenMapApiWithLogicalLaneAndBoundaries_WhenConvertToGtGenMap_ThenLogicalLaneAndBoundariesAreConverted)
{
    // Arrange in SetUp
    // Act
    converter_->Convert();

    // Assert
    ASSERT_EQ(gtgen_map_.GetAll<map_api::LogicalLaneBoundary>().size(), 2);
    EXPECT_EQ(gtgen_map_.GetAll<map_api::LogicalLaneBoundary>().at(0).id, 1000);
    EXPECT_EQ(gtgen_map_.GetAll<map_api::LogicalLaneBoundary>().at(1).id, 1001);
    ASSERT_EQ(gtgen_map_.GetAll<map_api::LogicalLane>().size(), 2);
    EXPECT_EQ(gtgen_map_.GetAll<map_api::LogicalLane>().at(0).id, 1002);
    EXPECT_EQ(gtgen_map_.GetAll<map_api::LogicalLane>().at(1).id, 1003);

    const auto& logical_lane_boundary_1000 = gtgen_map_.GetAll<map_api::LogicalLaneBoundary>().at(0);
    const auto& logical_lane_boundary_1001 = gtgen_map_.GetAll<map_api::LogicalLaneBoundary>().at(1);
    const auto& logical_lane_1002 = gtgen_map_.GetAll<map_api::LogicalLane>().at(0);
    const auto& logical_lane_1003 = gtgen_map_.GetAll<map_api::LogicalLane>().at(1);

    ASSERT_EQ(logical_lane_boundary_1000.boundary_line.size(), 2);
    EXPECT_EQ(logical_lane_boundary_1000.boundary_line.at(0).s_position, 0_m);
    ASSERT_EQ(logical_lane_boundary_1001.boundary_line.size(), 2);
    EXPECT_EQ(logical_lane_boundary_1001.boundary_line.at(1).s_position, 1000_m);
    EXPECT_EQ(logical_lane_boundary_1000.passing_rule, map_api::LogicalLaneBoundary::PassingRule::kUnknown);
    EXPECT_EQ(logical_lane_boundary_1001.passing_rule, map_api::LogicalLaneBoundary::PassingRule::kUnknown);

    EXPECT_EQ(logical_lane_1002.move_direction, map_api::LogicalLane::MoveDirection::kUnknown);
    EXPECT_EQ(logical_lane_1002.type, map_api::LogicalLane::Type::kUnknown);
    EXPECT_EQ(logical_lane_1002.start_s, 0_m);
    EXPECT_EQ(logical_lane_1002.end_s, 1000_m);

    EXPECT_EQ(logical_lane_1003.move_direction, map_api::LogicalLane::MoveDirection::kUnknown);
    EXPECT_EQ(logical_lane_1003.type, map_api::LogicalLane::Type::kUnknown);
    EXPECT_EQ(logical_lane_1003.start_s, 0_m);
    EXPECT_EQ(logical_lane_1003.end_s, 1000_m);
}

}  // namespace gtgen::core::environment::map::map
