/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GtGenEnvironment/gtgen_environment.h"

#include "Core/Environment/DataStore/test_utils.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/GtGenEnvironment/Internal/i_map_engine.h"
#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/Utility/clock.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/MockMapEngine/mock_map_engine.h"
#include "Core/Tests/TestUtils/convert_first_custom_traffic_command_of_host_vehicle_to_route.h"

#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_controller_config.h>
#include <gtest/gtest.h>

namespace gtgen::core::environment::api
{
using units::literals::operator""_ms;
using units::literals::operator""_m;

std::unique_ptr<GtGenEnvironment> CreateEnvironmentWithDummyMap(
    const fs::path& map_file_path = "external/Dummy.xodr",
    const fs::path& map_model_reference = "",
    const service::user_settings::UserSettings& user_settings = service::user_settings::UserSettings{})
{
    auto map_engine = std::make_unique<test_utils::MockMapEngine>();
    map_engine->SetMap(test_utils::MapCatalogue::MapStraightRoad2km());
    auto env = std::make_unique<GtGenEnvironment>(user_settings, 40_ms);
    env->SetMapEngine(std::move(map_engine));
    env->Init();
    env->CreateMap(map_file_path, {}, map_model_reference);
    return env;
}

TEST(EnvironmentApiControllerTest, GivenEnvironment_WhenCreateController_ThenCreationSucceeds)
{
    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();

    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    EXPECT_NO_THROW(env.GetControllerRepository().Create(0, std::move(config)));
}

TEST(EnvironmentApiControllerTest, GivenEnvironmentControllerAndEntity_WhenAddingEntityToController_ThenAddingSucceeds)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    auto& entity_repository = env.GetEntityRepository();

    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();
    ASSERT_NO_THROW(env.GetControllerRepository().Create(0, std::move(config)));
    ASSERT_NO_THROW(entity_repository.Create(0, "vehicle", mantle_api::VehicleProperties{}));

    EXPECT_NO_THROW(env.AddEntityToController(entity_repository.Get(0)->get(), 0));
}

TEST(EnvironmentApiControllerTest, GivenEnvironmentWithoutController_WhenAddingEntityToController_ThenExceptionThrown)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    auto& entity_repository = env.GetEntityRepository();

    EXPECT_THROW(env.AddEntityToController(entity_repository.Get(0)->get(), 0), EnvironmentException);
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithCompositeControllerWithEntity_WhenUpdatingControlStrategies_ThenUpdateSucceeds)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 0;
    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    mantle_api::UniqueId entity_id = 10;
    auto& entity_repository = env.GetEntityRepository();
    ASSERT_NO_THROW(env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{}));
    EXPECT_NO_THROW(env.AddEntityToController(entity_repository.Get(entity_id)->get(), controller_id));

    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
    control_strategies.emplace_back(std::make_unique<mantle_api::KeepVelocityControlStrategy>());
    EXPECT_NO_THROW(env.UpdateControlStrategies(entity_id, control_strategies));
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithExternalCompositeControllerWithEntity_WhenUpdatingControlStrategies_ThenControlUnitsUnchanged)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    env.Init();

    std::uint64_t controller_id = 0;
    auto config = std::make_unique<mantle_api::ExternalControllerConfig>();
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    mantle_api::UniqueId entity_id = 10;
    auto& entity_repository = env.GetEntityRepository();
    ASSERT_NO_THROW(env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{}));
    EXPECT_NO_THROW(env.AddEntityToController(entity_repository.Get(entity_id)->get(), controller_id));

    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
    control_strategies.emplace_back(std::make_shared<mantle_api::KeepVelocityControlStrategy>());
    EXPECT_NO_THROW(env.UpdateControlStrategies(entity_id, control_strategies));

    auto controllers = env.GetActiveControllerRepository()->GetControllersByEntityId(entity_id);
    ASSERT_EQ(controllers.size(), 1);
    auto& control_units = controllers.front()->GetControlUnits();
    ASSERT_EQ(2, control_units.size());
    EXPECT_EQ(control_units[0]->GetName(), "HostVehicleInterfaceControlUnit");
    EXPECT_EQ(control_units[1]->GetName(), "VehicleModelControlUnit");
}

TEST(
    EnvironmentApiControllerTest,
    GivenEnvironmentWithExternalCompositeControllerWithEntity_WhenUpdatingControlStrategiesAndStep_ThenTrafficCommandAvailable)
{
    auto map_engine = std::make_unique<test_utils::MockMapEngine>();
    auto map = test_utils::MapCatalogue::MapStraightRoad2km();
    test_utils::MockCoordinateConverter* coordinate_converter =
        dynamic_cast<test_utils::MockCoordinateConverter*>(map->coordinate_converter.get());
    EXPECT_CALL(*coordinate_converter, Convert(testing::_)).Times(1);
    map_engine->SetMap(std::move(map));

    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    env.SetMapEngine(std::move(map_engine));
    env.Init();

    const fs::path maps_path{"external"};
    env.CreateMap(maps_path / "Dummy.xodr", {});

    auto config = std::make_unique<mantle_api::ExternalControllerConfig>();
    auto& controller = env.GetControllerRepository().Create(std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    auto& entity_repository = env.GetEntityRepository();
    mantle_api::VehicleProperties vehicle_properties;
    vehicle_properties.is_host = true;
    auto& entity = entity_repository.Create("Host", vehicle_properties);
    EXPECT_NO_THROW(env.AddEntityToController(entity_repository.Get("Host")->get(), controller.GetUniqueId()));

    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
    control_strategies.emplace_back(std::make_shared<mantle_api::FollowVelocitySplineControlStrategy>());
    EXPECT_NO_THROW(env.UpdateControlStrategies(entity.GetUniqueId(), control_strategies));
    env.Step(0_ms);
    EXPECT_EQ(1, env.GetTrafficCommands().size());
}

TEST(EnvironmentApiControllerTest, GivenEnvironmentWithoutActiveController_WhenUpdatingControlStrategies_ThenThrow)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
    control_strategies.emplace_back(std::make_unique<mantle_api::KeepVelocityControlStrategy>());
    EXPECT_THROW(env.UpdateControlStrategies(0, control_strategies), EnvironmentException);
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithInternalControllerWithoutControlUnit_WhenAskingGoalReached_ThenThrow)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_THROW(env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kKeepVelocity),
                 EnvironmentException);
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithInternalCompositeControllerWithEntity_WhenAssignRoute_ThenControllerContainsPathControlUnit)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 0;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    mantle_api::UniqueId entity_id = 10;
    auto& entity_repository = env.GetEntityRepository();
    env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity_repository.Get(entity_id)->get(), controller_id);
    auto composite_controllers = env.GetActiveControllerRepository()->GetControllersByEntityId(entity_id);
    ASSERT_EQ(composite_controllers.size(), 1);
    EXPECT_EQ(composite_controllers.front()->GetControlUnits().size(), 0);

    mantle_api::RouteDefinition route_definition;
    route_definition.waypoints.push_back({{0_m, 0_m, 0_m}, mantle_api::RouteStrategy::kShortest});
    route_definition.waypoints.push_back({{10_m, 0_m, 0_m}, mantle_api::RouteStrategy::kShortest});
    EXPECT_NO_THROW(env.AssignRoute(entity_id, route_definition));

    auto& control_units = composite_controllers.front()->GetControlUnits();
    ASSERT_EQ(1, control_units.size());
    EXPECT_EQ(control_units[0]->GetName(), "PathControlUnit");
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithHostVehicleInterfaceControlUnit_WhenAssignRoute_ThenHostVehicleTrafficCommandsContainRoute)
{
    auto env = CreateEnvironmentWithDummyMap();

    mantle_api::UniqueId entity_id = 0;
    mantle_api::VehicleProperties vehicle_properties;
    vehicle_properties.is_host = true;
    auto& entity = env->GetEntityRepository().Create(entity_id, "vehicle", vehicle_properties);
    auto& controller = env->GetControllerRepository().Create(std::make_unique<mantle_api::ExternalControllerConfig>());
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);
    env->AddEntityToController(entity, controller.GetUniqueId());
    mantle_api::RouteDefinition route_definition;
    route_definition.waypoints.push_back({{0_m, 0_m, 0_m}, mantle_api::RouteStrategy::kShortest});
    route_definition.waypoints.push_back({{0_m, 100_m, 0_m}, mantle_api::RouteStrategy::kShortest});

    EXPECT_NO_THROW(env->AssignRoute(entity_id, route_definition));
    env->Step(0_ms);
    const auto actual_path =
        gtgen::core::test_utils::ConvertFirstCustomTrafficCommandOfHostVehicleToRoute(env->GetTrafficCommands());
    EXPECT_FALSE(actual_path.empty());
}

TEST(EnvironmentApiControllerTest, GivenUserSettingsThatDoNotRequireToLogCyclics_WhenExecuting_ThenDoNotLogTheCyclics)
{
    // Given
    const fs::path output_path = fs::temp_directory_path();
    const std::string cyclics_filename = "Cyclics_Run_000.csv";
    const fs::path output_file_path = output_path / cyclics_filename;
    service::user_settings::UserSettings user_settings;
    user_settings.simulation_results.output_directory_path = output_path;
    user_settings.simulation_results.log_cyclics = false;
    auto env = CreateEnvironmentWithDummyMap("external/Dummy.xodr", "", user_settings);
    mantle_api::UniqueId entity_id = 0;
    mantle_api::VehicleProperties vehicle_properties;
    vehicle_properties.is_host = true;
    auto& entity = env->GetEntityRepository().Create(entity_id, "vehicle", vehicle_properties);
    auto& controller = env->GetControllerRepository().Create(std::make_unique<mantle_api::ExternalControllerConfig>());
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);
    env->AddEntityToController(entity, controller.GetUniqueId());

    // When
    env->Step(0_ms);
    env.reset();

    // Then
    ASSERT_FALSE(fs::exists(output_file_path)) << "File exists even when not required: " << (output_file_path).string();
}

TEST(EnvironmentApiControllerTest, GivenUserSettingsThatDoRequireToLogCyclics_WhenExecuting_ThenLogTheCyclics)
{
    // Given
    const fs::path output_path = fs::temp_directory_path();
    const std::string cyclics_filename = "Cyclics_Run_000.csv";
    const fs::path expected_output_file_path = output_path / cyclics_filename;
    const std::vector<std::string> expected_csv{
        "Timestep, 00:AccelerationEgo, 00:IndicatorState, 00:PitchAngle, 00:PositionRoute, 00:Road, 00:RollAngle, "
        "00:TCoordinate, 00:VelocityEgo, 00:XPosition, 00:YPosition, 00:YawAngle, 00:ZPosition",
        "40, 0.000000, 2, 0.000000, nan, nan, 0.000000, nan, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000"};
    service::user_settings::UserSettings user_settings;
    user_settings.simulation_results.output_directory_path = output_path;
    user_settings.simulation_results.log_cyclics = true;
    auto env = CreateEnvironmentWithDummyMap("external/Dummy.xodr", "", user_settings);
    mantle_api::UniqueId entity_id = 0;
    mantle_api::VehicleProperties vehicle_properties;
    vehicle_properties.is_host = true;
    auto& entity = env->GetEntityRepository().Create(entity_id, "vehicle", vehicle_properties);
    auto& controller = env->GetControllerRepository().Create(std::make_unique<mantle_api::ExternalControllerConfig>());
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);
    env->AddEntityToController(entity, controller.GetUniqueId());

    // When
    env->Step(0_ms);
    env.reset();

    // Then
    ASSERT_TRUE(fs::exists(expected_output_file_path))
        << "File does not exist: " << (expected_output_file_path).string();
    EXPECT_EQ(expected_csv, gtgen::core::environment::datastore::test_utils::readCSV(expected_output_file_path));
    fs::remove(expected_output_file_path);
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithControllerForKeepVelocityControlStrategy_WhenAskingGoalReached_ThenReturnFalse)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::KeepVelocityControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_FALSE(env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kKeepVelocity));
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithControllerForKeepLaneOffsetControlStrategy_WhenAskingGoalReached_ThenReturnFalse)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::KeepLaneOffsetControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_FALSE(env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kKeepLaneOffset));
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithControllerForFollowEmptyHeadingSplineControlStrategy_WhenAskingGoalReached_ThenReturnTrue)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::FollowHeadingSplineControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_TRUE(
        env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kFollowHeadingSpline));
}

TEST(
    EnvironmentApiControllerTest,
    GivenEnvironmentWithControllerForFollowEmptyLateralOffsetSplineControlStrategy_WhenAskingGoalReached_ThenReturnTrue)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::FollowLateralOffsetSplineControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_TRUE(
        env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kFollowLateralOffsetSpline));
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithControllerForFollowEmptyVelocitySplineControlStrategy_WhenAskingGoalReached_ThenReturnTrue)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::FollowVelocitySplineControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_TRUE(
        env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kFollowVelocitySpline));
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithControllerForAcquireLaneOffsetControlStrategy_WhenAskingGoalReached_ThenThrow)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::AcquireLaneOffsetControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_THROW(env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kAcquireLaneOffset),
                 EnvironmentException);
}

TEST(EnvironmentApiControllerTest, GivenEnvironment_WhenSettingTimeInEnvironment_ThenEnvironmentReturnsCorrectTime)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    service::utility::Clock::Instance().SetNow(mantle_api::Time{0});

    EXPECT_EQ(0_ms, env.GetSimulationTime());

    service::utility::Clock::Instance().SetNow(mantle_api::Time{100});
    EXPECT_EQ(100_ms, env.GetSimulationTime());

    service::utility::Clock::Instance().SetNow(mantle_api::Time{150});
    EXPECT_EQ(150_ms, env.GetSimulationTime());
}

TEST(EnvironmentApiControllerTest, GivenEnvironment_WhenCreateTpmControlUnit_ThenCreationSucceeds)
{
    auto config = std::make_unique<mantle_api::ExternalControllerConfig>();
    const fs::path maps_path{"external"};

    config->name = "traffic_participant_model_example";

    auto env = CreateEnvironmentWithDummyMap();
    EXPECT_NO_THROW(env->GetControllerRepository().Create(42, std::move(config)));
}

TEST(EnvironmentApiControllerTest, GivenEnvironment_WhenCreateExternalHostConfig_ThenCreationSucceeds)
{
    auto config = std::make_unique<mantle_api::ExternalControllerConfig>();
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    env.Init();

    EXPECT_NO_THROW(env.GetControllerRepository().Create(42, std::move(config)));
}

TEST(EnvironmentApiCustomCommandTest, GivenEnvironment_WhenCallExecuteCustomCommandWithoutActors_ThenNoThrow)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    EXPECT_NO_THROW(env.ExecuteCustomCommand({}, "MyType", "MyCommand"));
}

TEST(EnvironmentApiCustomCommandTest, GivenEnvironmentWithoutEntity_WhenCallExecuteCustomCommandWithActor_ThenThrow)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    EXPECT_ANY_THROW(env.ExecuteCustomCommand({"actor1"}, "MyType", "MyCommand"));
}

TEST(EnvironmentApiCustomCommandTest,
     GivenEnvironmentWithEntityWithoutController_WhenCallExecuteCustomCommandWithActor_ThenNoThrow)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    std::string entity_name = "host";
    env.GetEntityRepository().Create(0, entity_name, mantle_api::VehicleProperties{});
    EXPECT_NO_THROW(env.ExecuteCustomCommand({entity_name}, "MyType", "MyCommand"));
}

TEST(
    EnvironmentApiCustomCommandTest,
    GivenEnvironmentWithEntityAndActiveExternalController_WhenCallExecuteCustomCommandWithActor_ThenCreateTrafficCommand)
{
    auto env = CreateEnvironmentWithDummyMap();
    std::string entity_name = "host";
    mantle_api::VehicleProperties properties;
    properties.is_host = true;
    auto& entity = env->GetEntityRepository().Create(0, entity_name, properties);
    auto config = std::make_unique<mantle_api::ExternalControllerConfig>();
    auto controller_id = 42;
    auto& controller = env->GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);
    env->AddEntityToController(entity, controller_id);

    env->ExecuteCustomCommand({entity_name}, "MyType", "MyCommand");
    env->Step(0_ms);
    EXPECT_EQ(env->GetTrafficCommands().size(), 1);
}

TEST(
    EnvironmentApiCustomCommandTest,
    GivenEnvironmentWithEntityAndActiveInternalController_WhenCallExecuteCustomCommandWithActor_ThenCreateNoTrafficCommand)
{
    auto env = CreateEnvironmentWithDummyMap();
    std::string entity_name = "host";
    mantle_api::VehicleProperties properties;
    properties.is_host = true;
    auto& entity = env->GetEntityRepository().Create(0, entity_name, properties);
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    auto controller_id = 42;
    auto& controller = env->GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);
    env->AddEntityToController(entity, controller_id);

    env->ExecuteCustomCommand({entity_name}, "MyType", "MyCommand");
    env->Step(0_ms);
    EXPECT_EQ(env->GetTrafficCommands().size(), 0);
}

TEST(EnvironmentApiUserDefinedValueTest, GivenEnvironment_WhenUserDefinedValueNotSet_ThenGetReturnsNoValue)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    EXPECT_FALSE(env.GetUserDefinedValue("MyName").has_value());
}

TEST(EnvironmentApiUserDefinedValueTest, GivenEnvironment_WhenUserDefinedValueSet_ThenGetReturnsValue)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    env.SetUserDefinedValue("MyName", "MyValue");
    ASSERT_TRUE(env.GetUserDefinedValue("MyName").has_value());
    EXPECT_EQ("MyValue", env.GetUserDefinedValue("MyName").value());
}

TEST(EnvironmentApiControllerTest, GivenEnvironment_WhenMapWithModelReference_ThenModelReferenceInGtGenMapFilled)
{
    const std::string expected_model_reference = "DummyMapModelReference";
    auto env = CreateEnvironmentWithDummyMap("DummyMapPath.xodr", expected_model_reference);

    EXPECT_EQ(env->GetGtGenMap().GetModelReference(), expected_model_reference);
}

}  // namespace gtgen::core::environment::api
