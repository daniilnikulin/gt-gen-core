cc_library(
    name = "gtgen_environment",
    srcs = ["gtgen_environment.cpp"],
    hdrs = ["gtgen_environment.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Communication/Dispatchers:__subpackages__",
        "//Core/Environment/Chunking:__subpackages__",
        "//Core/Environment/Controller:__subpackages__",
        "//Core/Environment/GtGenEnvironment/Internal:__subpackages__",
        "//Core/Simulation/Simulator:__subpackages__",
    ],
    deps = [
        ":entity_repository",
        ":i_gtgen_environment",
        "//Core/Environment/Chunking:chunking",
        "//Core/Environment/Controller:external_controller_config_converter",
        "//Core/Environment/DataStore:basic_data_buffer_implementation",
        "//Core/Environment/DataStore:data_buffer_interface",
        "//Core/Environment/DataStore:output_generator",
        "//Core/Environment/GroundTruth:sensor_view_builder",
        "//Core/Environment/GtGenEnvironment/Internal:active_controller_repository",
        "//Core/Environment/GtGenEnvironment/Internal:controller_prototypes",
        "//Core/Environment/GtGenEnvironment/Internal:coordinate_converter",
        "//Core/Environment/GtGenEnvironment/Internal:geometry_helper",
        "//Core/Environment/GtGenEnvironment/Internal:i_map_engine",
        "//Core/Environment/GtGenEnvironment/Internal:lane_assignment_service",
        "//Core/Environment/Host:host_vehicle_interface",
        "//Core/Environment/Host:host_vehicle_model",
        "//Core/Environment/Map/Common:converter_utility",
        "//Core/Environment/TrafficCommand:traffic_command_builder",
        "//Core/Environment/TrafficSwarm/Xosc:traffic_swarm_xosc",
        "//Core/Service/Utility:clock",
        "@mantle_api",
    ],
)

cc_test(
    name = "gtgen_environment_test",
    timeout = "short",
    srcs = ["gtgen_environment_test.cpp"],
    copts = ["-Wno-deprecated-declarations"],  # depreacted  IController& mantle_api::Create()
    data = [
        "//:sanitizer.supp",
        "//Core/Environment/Controller/Internal/ControlUnits:traffic_participant_model_example.so",
    ],
    env = {"LSAN_OPTIONS": "suppressions=sanitizer.supp"},
    deps = [
        ":gtgen_environment",
        "//Core/Environment/DataStore:test_utils",
        "//Core/Environment/GtGenEnvironment/Internal:i_map_engine",
        "//Core/Service/Utility:clock",
        "//Core/Tests/TestUtils:convert_first_custom_traffic_command_of_host_vehicle_to_route",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "//Core/Tests/TestUtils/MockMapEngine:mock_map_engine",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "entity_repository",
    srcs = ["entity_repository.cpp"],
    hdrs = ["entity_repository.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Communication/AsyncServer:__subpackages__",
        "//Core/Communication/Dispatchers:__subpackages__",
        "//Core/Communication/Serialization:__subpackages__",
        "//Core/Environment/Chunking:__subpackages__",  # used by tests only
        "//Core/Environment/Controller:__subpackages__",
        "//Core/Environment/GroundTruth:__subpackages__",
        "//Core/Environment/GtGenEnvironment/Internal:__subpackages__",
        "//Core/Environment/TrafficSwarm/Xosc:__subpackages__",
        "//Core/Service/Utility:__subpackages__",
        "//Core/Tests/TestUtils/ProtoUtils:__subpackages__",
    ],
    deps = [
        "//Core/Environment/Entities:entities",
        "//Core/Environment/Exception:exception",
        "//Core/Environment/GtGenEnvironment/Internal:entity_producer",
        "//Core/Service/Logging:logging",
        "//Core/Service/Utility:algorithm_utils",
    ],
)

cc_test(
    name = "entity_repository_test",
    timeout = "short",
    srcs = ["entity_repository_test.cpp"],
    deps = [
        ":entity_repository",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "environment_factory",
    srcs = ["environment_factory.cpp"],
    hdrs = ["environment_factory.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Tests:__subpackages__",
    ],
    deps = [
        ":gtgen_environment",
        "//Core/Environment/GtGenEnvironment/Internal:i_map_engine",
        "//Core/Service/UserSettings:user_settings",
        "@mantle_api",
    ],
)

cc_test(
    name = "environment_factory_test",
    timeout = "short",
    srcs = ["environment_factory_test.cpp"],
    deps = [
        ":environment_factory",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "i_gtgen_environment",
    hdrs = ["i_gtgen_environment.h"],
    deps = [
        ":entity_repository",
        "//Core/Environment/Chunking:chunking",
        "//Core/Environment/GroundTruth:sensor_view_builder",
        "//Core/Environment/Host:host_vehicle_interface",
        "//Core/Environment/Host:host_vehicle_model",
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "@//third_party/open_simulation_interface:open_simulation_interface",
        "@mantle_api",
    ],
)
