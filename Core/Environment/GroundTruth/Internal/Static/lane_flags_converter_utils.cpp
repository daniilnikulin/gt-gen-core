/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/lane_flags_converter_utils.h"

namespace gtgen::core::environment::proto_groundtruth
{

osi3::Lane_Classification_Type GetLaneFlagType(const map::LaneFlags& lane_flags)
{
    if (lane_flags.IsDrivable())
    {
        return osi3::Lane_Classification_Type_TYPE_DRIVING;
    }

    return osi3::Lane_Classification_Type_TYPE_NONDRIVING;
}

osi3::Lane_Classification_Subtype GetLaneFlagSubType(const map::LaneFlags& lane_flags)
{
    osi3::Lane_Classification_Subtype subtype{osi3::Lane_Classification_Subtype_SUBTYPE_OTHER};

    // Note: Only one subtype can be set. See:
    // https://opensimulationinterface.github.io/open-simulation-interface/structosi3_1_1Lane_1_1Classification.html#ac5315bb0a34763ca7e0a31ca72a3350d
    // There are combinations, which do not make sense together (i.e. Bus + Parking, Merge + Split,...). We assume
    // the actual map does provide sensible flags. However, a shoulder could also be in an entry or exit lane.
    // In this case the shoulder flag should have priority over the entry/exit, because a shoulder is more likely to
    // not be driven on.
    if (lane_flags.IsShoulderLane())
    {
        subtype = osi3::Lane_Classification_Subtype_SUBTYPE_SHOULDER;
    }
    else if (lane_flags.IsMergeLane() || lane_flags.IsEntryLane())
    {
        subtype = osi3::Lane_Classification_Subtype_SUBTYPE_ENTRY;
    }
    else if (lane_flags.IsSplitLane() || lane_flags.IsExitLane())
    {
        subtype = osi3::Lane_Classification_Subtype_SUBTYPE_EXIT;
    }
    else if (lane_flags.IsNormalLane())
    {
        subtype = osi3::Lane_Classification_Subtype_SUBTYPE_NORMAL;
    }
    else if (lane_flags.IsParking())
    {
        subtype = osi3::Lane_Classification_Subtype_SUBTYPE_PARKING;
    }
    else if (lane_flags.IsBicycle())
    {
        subtype = osi3::Lane_Classification_Subtype_SUBTYPE_BIKING;
    }
    else if (lane_flags.IsBus())
    {
        // Cannot be reflected at the moment
        subtype = osi3::Lane_Classification_Subtype_SUBTYPE_OTHER;
    }
    else if (lane_flags.IsContinue())
    {
        // Cannot be reflected at the moment
        subtype = osi3::Lane_Classification_Subtype_SUBTYPE_OTHER;
    }
    else if (lane_flags.IsFullyAttributed())
    {
        // Cannot be reflected at the moment
        subtype = osi3::Lane_Classification_Subtype_SUBTYPE_OTHER;
    }

    return subtype;
}

}  // namespace gtgen::core::environment::proto_groundtruth
