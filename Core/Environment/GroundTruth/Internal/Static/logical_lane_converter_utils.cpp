/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/logical_lane_converter_utils.h"

#include "osi_logicallane.pb.h"

#include <MapAPI/logical_lane.h>

namespace gtgen::core::environment::proto_groundtruth
{

void FillLogicalLaneId(const map_api::LogicalLane* const gtgen_logical_lane,
                       osi3::LogicalLane* const proto_logical_lane)
{
    proto_logical_lane->mutable_id()->set_value(gtgen_logical_lane->id);
}

void FillLogicalLaneType(const map_api::LogicalLane* const gtgen_logical_lane,
                         osi3::LogicalLane* const proto_logical_lane)
{
    proto_logical_lane->set_type(static_cast<::osi3::LogicalLane_Type>(gtgen_logical_lane->type));
}

void FillLogicalLaneSourceReferences(const map_api::LogicalLane* const gtgen_logical_lane,
                                     osi3::LogicalLane* const proto_logical_lane)
{
    for (const auto& source_reference : gtgen_logical_lane->source_references)
    {
        auto proto_source_reference = proto_logical_lane->add_source_reference();
        proto_source_reference->set_reference(source_reference.reference);
        proto_source_reference->set_type(source_reference.type);
        for (const auto& identifier : source_reference.identifiers)
        {
            proto_source_reference->add_identifier(identifier);
        }
    }
}

void FillLogicalLanePhysicalLaneReferences(const map_api::LogicalLane* const gtgen_logical_lane,
                                           osi3::LogicalLane* const proto_logical_lane,
                                           const std::unordered_set<mantle_api::UniqueId>& existing_lane_ids)
{
    for (const auto& physical_lane_reference : gtgen_logical_lane->physical_lane_references)
    {
        if (!existing_lane_ids.count(physical_lane_reference.physical_lane.id))
        {
            continue;
        }
        auto proto_physical_lane_reference = proto_logical_lane->add_physical_lane_reference();
        proto_physical_lane_reference->mutable_physical_lane_id()->set_value(physical_lane_reference.physical_lane.id);
        proto_physical_lane_reference->set_start_s(physical_lane_reference.start_s.value());
        proto_physical_lane_reference->set_end_s(physical_lane_reference.end_s.value());
    }
}

void FillLogicalLaneReferenceLine(const map_api::LogicalLane* const gtgen_logical_lane,
                                  osi3::LogicalLane* const proto_logical_lane,
                                  const std::unordered_set<mantle_api::UniqueId>& existing_reference_line_ids)
{
    if ((gtgen_logical_lane->reference_line != nullptr) &&
        existing_reference_line_ids.count(gtgen_logical_lane->reference_line->id))
    {
        proto_logical_lane->mutable_reference_line_id()->set_value(gtgen_logical_lane->reference_line->id);
        proto_logical_lane->set_start_s(gtgen_logical_lane->start_s.value());
        proto_logical_lane->set_end_s(gtgen_logical_lane->end_s.value());
        proto_logical_lane->set_move_direction(
            static_cast<::osi3::LogicalLane_MoveDirection>(gtgen_logical_lane->move_direction));
    }
}

void FillLogicalLaneRelationships(const map_api::LogicalLane* const gtgen_logical_lane,
                                  osi3::LogicalLane* const proto_logical_lane,
                                  const std::unordered_set<mantle_api::UniqueId>& existing_logical_lane_ids)
{
    auto fill_logical_lane_relationships = [&existing_logical_lane_ids](const auto& related_gtgen_logical_lanes,
                                                                        auto add_proto_logical_lane_to_fill) {
        for (const auto& related_gtgen_logical_lane : related_gtgen_logical_lanes)
        {
            if (existing_logical_lane_ids.count(related_gtgen_logical_lane.other_lane.id))
            {
                auto proto_logical_lane_to_fill = add_proto_logical_lane_to_fill();
                proto_logical_lane_to_fill->mutable_other_lane_id()->set_value(
                    related_gtgen_logical_lane.other_lane.id);
                proto_logical_lane_to_fill->set_start_s(related_gtgen_logical_lane.start_s.value());
                proto_logical_lane_to_fill->set_end_s(related_gtgen_logical_lane.end_s.value());
                proto_logical_lane_to_fill->set_start_s_other(related_gtgen_logical_lane.start_s_other.value());
                proto_logical_lane_to_fill->set_end_s_other(related_gtgen_logical_lane.end_s_other.value());
            }
        }
    };

    fill_logical_lane_relationships(gtgen_logical_lane->right_adjacent_lanes,
                                    [proto_logical_lane]() { return proto_logical_lane->add_right_adjacent_lane(); });
    fill_logical_lane_relationships(gtgen_logical_lane->left_adjacent_lanes,
                                    [&proto_logical_lane]() { return proto_logical_lane->add_left_adjacent_lane(); });
    fill_logical_lane_relationships(gtgen_logical_lane->overlapping_lanes,
                                    [&proto_logical_lane]() { return proto_logical_lane->add_overlapping_lane(); });
}

void FillLogicalLaneLogicalLaneBoundaries(const map_api::LogicalLane* const gtgen_logical_lane,
                                          osi3::LogicalLane* const proto_logical_lane)
{
    for (const auto& right_boundary : gtgen_logical_lane->right_boundaries)
    {
        auto proto_right_boundary_id = proto_logical_lane->add_right_boundary_id();
        proto_right_boundary_id->set_value(right_boundary.get().id);
    }

    for (const auto& left_boundary : gtgen_logical_lane->left_boundaries)
    {
        auto proto_left_boundary_id = proto_logical_lane->add_left_boundary_id();
        proto_left_boundary_id->set_value(left_boundary.get().id);
    }
}

void FillLogicalLaneConnections(const map_api::LogicalLane* const gtgen_logical_lane,
                                osi3::LogicalLane* const proto_logical_lane,
                                const std::unordered_set<mantle_api::UniqueId>& existing_logical_lane_ids)
{
    auto fill_logical_lane_connections = [&existing_logical_lane_ids](const auto& connected_gtgen_logical_lanes,
                                                                      auto add_proto_logical_lane_to_fill) {
        for (const auto& connected_gtgen_logical_lane : connected_gtgen_logical_lanes)
        {
            if (existing_logical_lane_ids.count(connected_gtgen_logical_lane.get().id))
            {
                auto proto_logical_lane_to_fill = add_proto_logical_lane_to_fill();
                proto_logical_lane_to_fill->mutable_other_lane_id()->set_value(connected_gtgen_logical_lane.get().id);
                // TODO: map_api does not define an equivalent of osi3::LogicalLane_LaneConnection
                // therefore, unable to set the `at_begin_of_other_lane_` here.
            }
        }
    };

    fill_logical_lane_connections(gtgen_logical_lane->predecessor_lanes,
                                  [&proto_logical_lane]() { return proto_logical_lane->add_predecessor_lane(); });
    fill_logical_lane_connections(gtgen_logical_lane->successor_lanes,
                                  [&proto_logical_lane]() { return proto_logical_lane->add_successor_lane(); });
}

void FillLogicalLaneStreetName(const map_api::LogicalLane* const gtgen_logical_lane,
                               osi3::LogicalLane* const proto_logical_lane)
{
    proto_logical_lane->set_street_name(gtgen_logical_lane->street_name);
}

}  // namespace gtgen::core::environment::proto_groundtruth
