/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "Core/Environment/DataStore/cyclics.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace gtgen::core::environment::datastore
{

using ::testing::ElementsAre;
using ::testing::Eq;

TEST(Cyclics_Test, GetHeader_ReturnsCorrectHeader)
{
    Cyclics cyclics;
    cyclics.Insert(0, "ParameterA", "123");
    cyclics.Insert(0, "ParameterC", "234");
    cyclics.Insert(100, "ParameterA", "345");
    cyclics.Insert(100, "ParameterB", "456");

    std::string header = cyclics.GetHeader();
    ASSERT_THAT(header, Eq("ParameterA, ParameterB, ParameterC"));
}

TEST(Cyclics_Test, GetTimeSteps_ReturnsCorrectTimesteps)
{
    Cyclics cyclics;
    cyclics.Insert(0, "ParameterA", "123");
    cyclics.Insert(0, "ParameterC", "234");
    cyclics.Insert(100, "ParameterA", "345");
    cyclics.Insert(150, "ParameterB", "456");

    const auto& timesteps = cyclics.GetTimeSteps();
    ASSERT_THAT(timesteps, ElementsAre(0, 100, 150));
}

TEST(Cyclics_Test, GetSamplesLineAllSamplesExisting_ReturnsCorrectLine)
{
    Cyclics cyclics;
    cyclics.Insert(0, "ParameterA", "123");
    cyclics.Insert(0, "ParameterC", "234");
    cyclics.Insert(0, "ParameterB", "345");
    cyclics.Insert(100, "ParameterA", "456");
    cyclics.Insert(100, "ParameterC", "567");
    cyclics.Insert(100, "ParameterB", "678");

    std::string samples_line = cyclics.GetSamplesLine(0);
    ASSERT_THAT(samples_line, Eq("123, 345, 234"));
    samples_line = cyclics.GetSamplesLine(1);
    ASSERT_THAT(samples_line, Eq("456, 678, 567"));
}

TEST(Cyclics_Test, GetSamplesLineSamplesAddedAfter_ReturnsLineWithEmptyString)
{
    Cyclics cyclics;
    cyclics.Insert(0, "ParameterA", "123");
    cyclics.Insert(100, "ParameterA", "234");
    cyclics.Insert(100, "ParameterC", "345");
    cyclics.Insert(200, "ParameterA", "456");
    cyclics.Insert(200, "ParameterC", "567");
    cyclics.Insert(200, "ParameterB", "678");

    std::string samples_line = cyclics.GetSamplesLine(0);
    ASSERT_THAT(samples_line, Eq("123, , "));
    samples_line = cyclics.GetSamplesLine(1);
    ASSERT_THAT(samples_line, Eq("234, , 345"));
    samples_line = cyclics.GetSamplesLine(2);
    ASSERT_THAT(samples_line, Eq("456, 678, 567"));
}

TEST(Cyclics_Test, GetSamplesLineSamplesMissingInBetween_ReturnsLineWithEmptyString)
{
    Cyclics cyclics;
    cyclics.Insert(0, "ParameterA", "123");
    cyclics.Insert(0, "ParameterC", "234");
    cyclics.Insert(0, "ParameterB", "345");
    cyclics.Insert(100, "ParameterA", "456");
    cyclics.Insert(200, "ParameterA", "567");
    cyclics.Insert(200, "ParameterC", "678");
    cyclics.Insert(200, "ParameterB", "789");

    std::string samples_line = cyclics.GetSamplesLine(1);
    ASSERT_THAT(samples_line, Eq("456, , "));
}

TEST(Cyclics_Test, GetSamplesLineSamplesNotUntilEnd_ReturnsLineWithEmptyString)
{
    Cyclics cyclics;
    cyclics.Insert(0, "ParameterA", "123");
    cyclics.Insert(0, "ParameterC", "234");
    cyclics.Insert(0, "ParameterB", "345");
    cyclics.Insert(100, "ParameterA", "456");
    cyclics.Insert(100, "ParameterB", "678");
    cyclics.Insert(200, "ParameterB", "789");

    std::string samples_line = cyclics.GetSamplesLine(0);
    ASSERT_THAT(samples_line, Eq("123, 345, 234"));
    samples_line = cyclics.GetSamplesLine(1);
    ASSERT_THAT(samples_line, Eq("456, 678, "));
    samples_line = cyclics.GetSamplesLine(2);
    ASSERT_THAT(samples_line, Eq(", 789, "));
}

}  // namespace gtgen::core::environment::datastore
