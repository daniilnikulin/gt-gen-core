/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "Core/Environment/DataStore/utils.h"

#include "Core/Environment/DataStore/data_buffer_interface.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace gtgen::core::environment::datastore
{

using std::literals::string_literals::operator""s;
class UtilsTestFixture : public testing::TestWithParam<std::tuple<Value, std::string>>
{
};

INSTANTIATE_TEST_SUITE_P(Utils,
                         UtilsTestFixture,
                         testing::ValuesIn(std::vector<std::tuple<Value, std::string>>{
                             std::make_tuple(true, "1"s),
                             std::make_tuple('#', std::to_string('#')),
                             std::make_tuple(123, "123"s),
                             std::make_tuple(std::size_t{123}, "123"s),
                             std::make_tuple(1.1f, "1.100000"s),
                             std::make_tuple(1.1, "1.100000"s),
                             std::make_tuple("abc"s, "abc"s),
                             std::make_tuple(std::vector<bool>{true, false}, "1,0"s),
                             std::make_tuple(std::vector<char>{'#', 'k'}, "#,k"s),
                             std::make_tuple(std::vector<int>{123, 456}, "123,456"s),
                             std::make_tuple(std::vector<std::size_t>{123, 456}, "123,456"s),
                             std::make_tuple(std::vector<float>{1.1, 2.2}, "1.1,2.2"s),
                             std::make_tuple(std::vector<double>{1.1, 2.2}, "1.1,2.2"s),
                             std::make_tuple(std::vector<std::string>{"abc", "def"}, "abc,def"s)}));

TEST_P(UtilsTestFixture, GivenVariantInput_WhenToString_ThenExpectedString)
{
    // Given
    const Value input = std::get<0>(GetParam());
    std::string expected_result = std::get<1>(GetParam());

    // When
    std::string result;
    std::visit(utils::to_string([&result](const std::string& value_str) { result = value_str; }), input);

    // Then
    EXPECT_EQ(result, expected_result);
}

}  // namespace gtgen::core::environment::datastore
