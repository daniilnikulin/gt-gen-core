/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Core/Environment/DataStore/data_buffer_interface.h"

namespace gtgen::core::environment::datastore
{

CyclicRow::CyclicRow(mantle_api::UniqueId id, Key k, Value v) : entity_id{id}, key{k}, value{v} {}

CyclicResultInterface::CyclicResultInterface() = default;

CyclicResultInterface::~CyclicResultInterface() = default;

DataBufferReadInterface::DataBufferReadInterface() = default;

DataBufferReadInterface::~DataBufferReadInterface() = default;

DataBufferWriteInterface::DataBufferWriteInterface() = default;

DataBufferWriteInterface::~DataBufferWriteInterface() = default;

DataBufferInterface::DataBufferInterface() = default;

DataBufferInterface::~DataBufferInterface() = default;

}  // namespace gtgen::core::environment::datastore
