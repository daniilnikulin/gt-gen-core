/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Core/Environment/DataStore/basic_data_buffer_implementation.h"

namespace gtgen::core::environment::datastore
{

CyclicResult::CyclicResult(CyclicRowRefs elements) : elements{std::move(elements)} {}

CyclicResult::~CyclicResult() = default;

std::size_t CyclicResult::size() const
{
    return elements.size();
}

const CyclicRow& CyclicResult::at(const std::size_t index) const
{
    return elements.at(index);
}

CyclicRowRefs::const_iterator CyclicResult::begin() const
{
    return elements.cbegin();
}

CyclicRowRefs::const_iterator CyclicResult::end() const
{
    return elements.cend();
}

BasicDataBufferImplementation::BasicDataBufferImplementation() = default;

BasicDataBufferImplementation::~BasicDataBufferImplementation() = default;

std::unique_ptr<CyclicResultInterface> BasicDataBufferImplementation::GetCyclic(const mantle_api::UniqueId entity_id,
                                                                                const Key& key) const
{
    CyclicRowRefs row_refs;

    for (const CyclicRow& store_value : cyclic_store_)
    {
        if (entity_id == store_value.entity_id && (key == store_value.key || key == kWildcard))
        {
            row_refs.emplace_back(store_value);
        }
    }

    return std::make_unique<CyclicResult>(row_refs);
}

std::unique_ptr<CyclicResultInterface> BasicDataBufferImplementation::GetCyclic(const Key& key) const
{
    CyclicRowRefs row_refs;

    for (const CyclicRow& store_value : cyclic_store_)
    {
        if (key == store_value.key || key == kWildcard)
        {
            row_refs.emplace_back(store_value);
        }
    }

    return std::make_unique<CyclicResult>(row_refs);
}

std::unique_ptr<CyclicResultInterface> BasicDataBufferImplementation::GetCyclic(
    const std::optional<mantle_api::UniqueId> entity_id,
    const Key& key) const
{
    return entity_id.has_value() ? GetCyclic(entity_id.value(), key) : GetCyclic(key);
}

void BasicDataBufferImplementation::PutCyclic(const mantle_api::UniqueId agentId, const Key& key, const Value& value)
{
    cyclic_store_.emplace_back(agentId, key, value);
}

void BasicDataBufferImplementation::Clear()
{
    cyclic_store_.clear();
}

}  // namespace gtgen::core::environment::datastore
