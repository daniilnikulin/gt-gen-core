/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Entities/traffic_sign_entity.h"

#include <gtest/gtest.h>

#include <memory>

using namespace gtgen::core::environment::entities;

class TrafficSignEntityTest : public ::testing::Test
{
  protected:
    void SetUp() override { traffic_sign = std::make_unique<TrafficSignEntity>(1, "MainSign"); }

    std::unique_ptr<TrafficSignEntity> traffic_sign;
};

TEST_F(TrafficSignEntityTest, GivenSupplementarySign_WhenAddedToMainSign_ThenAddedCorrectly)
{
    mantle_api::UniqueId supplementary_id = 2;
    auto supplementary_sign = std::make_unique<StaticObject>(supplementary_id, "SupplementarySign");

    traffic_sign->AddSupplementarySign(supplementary_id, *supplementary_sign);

    auto retrieved_sign = traffic_sign->GetSupplementarySign(supplementary_id);
    ASSERT_NE(retrieved_sign, nullptr);
    EXPECT_EQ(retrieved_sign->GetName(), "SupplementarySign");
}

TEST_F(TrafficSignEntityTest, GivenTwoSupplementarySigns_WhenAddedToMainSign_ThenAddedCorrectly)
{
    mantle_api::UniqueId id1 = 2, id2 = 3;
    auto sign1 = std::make_unique<StaticObject>(id1, "Sign1");
    auto sign2 = std::make_unique<StaticObject>(id2, "Sign2");

    traffic_sign->AddSupplementarySign(id1, *sign1);
    traffic_sign->AddSupplementarySign(id2, *sign2);

    auto supplementary_signs = traffic_sign->GetSupplementarySigns();
    EXPECT_EQ(supplementary_signs.size(), 2);
    EXPECT_NE(supplementary_signs.find(id1), supplementary_signs.end());
    EXPECT_NE(supplementary_signs.find(id2), supplementary_signs.end());
}

TEST_F(TrafficSignEntityTest, GivenNonExistentSupplementarySignId_WhenGetSupplementarySign_ThenReturnNull)
{
    mantle_api::UniqueId non_existent_id = 999;
    auto retrieved_sign = traffic_sign->GetSupplementarySign(non_existent_id);
    EXPECT_EQ(retrieved_sign, nullptr);
}

TEST_F(TrafficSignEntityTest, GivenValidSupplementarySignId_WhenDelete_ThenDeletedCorrectly)
{
    mantle_api::UniqueId id = 2;
    auto sign = std::make_unique<StaticObject>(id, "Sign");

    traffic_sign->AddSupplementarySign(id, *sign);
    EXPECT_TRUE(traffic_sign->DeleteSupplementarySign(id));
    EXPECT_EQ(traffic_sign->GetSupplementarySign(id), nullptr);
}

TEST_F(TrafficSignEntityTest, GivenNonExistentSupplementarySignId_WhenDelete_ThenReturnFalse)
{
    mantle_api::UniqueId non_existent_id = 999;
    EXPECT_FALSE(traffic_sign->DeleteSupplementarySign(non_existent_id));
}
