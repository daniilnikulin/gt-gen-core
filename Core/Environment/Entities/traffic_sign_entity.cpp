/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Entities/traffic_sign_entity.h"

namespace gtgen::core::environment::entities
{

TrafficSignEntity::TrafficSignEntity(mantle_api::UniqueId id, const std::string& name) : StaticObject(id, name) {}

void TrafficSignEntity::AddSupplementarySign(const mantle_api::UniqueId& key, mantle_api::IEntity& supplementary_sign)
{
    supplementary_signs.emplace(key, std::ref(supplementary_sign));
}

mantle_api::IEntity* TrafficSignEntity::GetSupplementarySign(const mantle_api::UniqueId& key) const
{
    if (auto it = supplementary_signs.find(key); it != supplementary_signs.end())
    {
        return &it->second.get();
    }
    return nullptr;
}

std::unordered_map<mantle_api::UniqueId, std::reference_wrapper<mantle_api::IEntity>>
TrafficSignEntity::GetSupplementarySigns() const
{
    return supplementary_signs;
}

bool TrafficSignEntity::DeleteSupplementarySign(const mantle_api::UniqueId& key)
{
    auto it = supplementary_signs.find(key);
    if (it != supplementary_signs.end())
    {
        supplementary_signs.erase(it);
        return true;
    }
    return false;
}

}  // namespace gtgen::core::environment::entities
