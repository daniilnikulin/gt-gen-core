/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_model_example.h"

#include <gtest/gtest.h>
#include <osi_common.pb.h>
#include <osi_trafficupdate.pb.h>

namespace gtgen::core::environment::controller
{

class TrafficParticipantModelExampleTest : public testing::Test
{
  public:
    TrafficParticipantModelExampleTest()
    {
        auto* new_moving_object = GetMutableGroundTruth().mutable_moving_object()->Add();
        new_moving_object->mutable_base()->mutable_position()->set_x(0.0);

        new_moving_object = GetMutableGroundTruth().mutable_moving_object()->Add();
        new_moving_object->mutable_base()->mutable_position()->set_x(10.0);
    }

    const osi3::GroundTruth& GetGroundTruth() const { return sensor_view_.global_ground_truth(); }

    osi3::GroundTruth& GetMutableGroundTruth() { return *sensor_view_.mutable_global_ground_truth(); }

    const osi3::SensorView& GetSensorView() const { return sensor_view_; }

  private:
    osi3::SensorView sensor_view_;
};

TEST_F(TrafficParticipantModelExampleTest, GivenTpm_WhenUpdate_ThenHasCorrectPosition)
{
    TpmExample tpm_example{};

    auto traffic_update_result =
        tpm_example.Update(std::chrono::milliseconds(0), GetSensorView(), osi3::TrafficCommand{});

    EXPECT_EQ(traffic_update_result.traffic_update.update(0).base().position().x(), 123.45);
}

}  // namespace gtgen::core::environment::controller
