/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_EXTERNALCONTROLLERFACTORY_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_EXTERNALCONTROLLERFACTORY_H

#include "Core/Environment/Controller/Internal/ControlUnits/host_vehicle_interface_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/recovery_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/vehicle_model_control_unit.h"
#include "Core/Environment/Controller/composite_controller.h"
#include "Core/Environment/Controller/external_controller_config.h"
#include "Core/Environment/Exception/exception.h"

#include <MantleAPI/Traffic/control_strategy.h>

namespace gtgen::core::environment::controller
{
class ExternalControllerFactory
{
  public:
    static void Create(controller::CompositeController* composite_controller,
                       mantle_api::ExternalControllerConfig* external_config)
    {
        if (auto* tpm_control_unit_config = dynamic_cast<TrafficParticipantControlUnitConfig*>(external_config))
        {
            composite_controller->AddControlUnit(std::make_unique<controller::TrafficParticipantControlUnit>(
                tpm_control_unit_config->parameters, *tpm_control_unit_config));
        }
        else if (auto* gtgen_external_config = dynamic_cast<GtGenExternalControllerConfig*>(external_config))
        {
            composite_controller->AddControlUnit(std::make_unique<controller::HostVehicleInterfaceControlUnit>(
                gtgen_external_config->waypoints,
                gtgen_external_config->map_query_service,
                gtgen_external_config->traffic_command_builder));
            composite_controller->AddControlUnit(std::make_unique<controller::VehicleModelControlUnit>(
                gtgen_external_config->host_vehicle_model, gtgen_external_config->gtgen_map));
            if (gtgen_external_config->recovery_mode_enabled)
            {
                composite_controller->AddControlUnit(std::make_unique<controller::RecoveryControlUnit>(
                    gtgen_external_config->host_interface, gtgen_external_config->map_query_service));
            }
        }
        else
        {
            throw EnvironmentException(
                "Tried to create a controller with an unsupported configuration. Please open an issue for "
                "further assistance.");
        }
    }
};
}  // namespace gtgen::core::environment::controller
#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_EXTERNALCONTROLLERFACTORY_H
