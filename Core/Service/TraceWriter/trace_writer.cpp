/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/TraceWriter/trace_writer.h"

#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Service/Logging/logging.h"

#include <boost/endian/conversion.hpp>

#include <string_view>

namespace gtgen::core::service
{

TraceWriter::TraceWriter(const gtgen::core::fs::path& trace_path) : trace_path_(trace_path)
{
    RemoveFileIfExist(trace_path_);
    SetFileExtension(trace_path_);
    trace_.open(trace_path_, std::ofstream::out | std::ofstream::app);
    if (!trace_.good())
    {
        LogAndThrow(std::runtime_error("Cannot open file: " + trace_path_.string()));
    }
}

TraceWriter::~TraceWriter()
{
    trace_.close();
}

void TraceWriter::WriteToTraceFile(const osi3::SensorView& sensor_view)
{
    if (!trace_.good())
    {
        LogAndThrow(std::runtime_error("TraceWriter cannot write to file:" + trace_path_.string()));
    }

    std::string serialized_data = sensor_view.SerializeAsString();

    if (extension_type_ == TraceWriter::ExtensionType::kOSI)
    {
        const std::uint32_t message_size = static_cast<std::uint32_t>(serialized_data.size());
        std::array<unsigned char, UINT_LENGTH> message_size_in_bytes;
        boost::endian::endian_store<std::uint32_t, UINT_LENGTH, boost::endian::order::little>(
            message_size_in_bytes.data(), message_size);
        trace_.write(reinterpret_cast<const char*>(message_size_in_bytes.data()), UINT_LENGTH);
        trace_ << serialized_data;
    }
    else
    {
        if (!first_call_)
        {
            trace_.write(TRACE_SEPARATOR.data(), TRACE_SEPARATOR.size());
        }
        first_call_ = false;
        trace_ << serialized_data;
    }

    trace_.flush();
}

void TraceWriter::SetFileExtension(const gtgen::core::fs::path& trace_path)
{
    auto trace_extension = gtgen::core::service::file_system::GetFileExtension(trace_path);

    if (trace_extension == "osi")
    {
        extension_type_ = ExtensionType::kOSI;
    }
    else if (trace_extension == "txt")
    {
        extension_type_ = ExtensionType::kTXT;
    }
    else
    {
        LogAndThrow(std::runtime_error("Trace file format not valid. Supported format are txt and osi."));
    }
}

void TraceWriter::RemoveFileIfExist(const gtgen::core::fs::path& trace_file)
{
    if (gtgen::core::fs::exists(trace_file))
    {
        gtgen::core::fs::remove(trace_file);
    }
}

}  // namespace gtgen::core::service
